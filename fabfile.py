# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.colors import green

env.user = 'ubuntu'
env.host_string = '67.205.138.55'
env.password = 'GrupKodeDev​@2014'
home_path = "/home/ubuntu"
settings = "--settings='glup.settings.production'"
activate_env = "source {}/glupvenv/bin/activate".format(home_path)
manage = "python manage.py"
settings_test = "--settings='glup.settings.test'"
settings_new = "--settings='glup.settings.new'"
activate_env_new = "source {}/glupnewEnv/bin/activate".format(home_path)


def deploy():
    print("Beginning Deploy:")
    with cd("{}/glup-backend".format(home_path)):
        run("git pull origin master")
        run("{} && pip install -r requirements.txt".format(activate_env))
        run("{} && {} collectstatic --noinput {}".format(activate_env, manage,
                                                         settings))
        run("{} && {} migrate {}".format(activate_env, manage, settings))
        sudo("service nginx restart", pty=False)
        sudo("service supervisor restart", pty=False)
    print(green("Deploy glup successful"))


def test():
    print("Beginning Glup Test Deploy:")
    with cd("{}/glup-test".format(home_path)):
        run("git pull origin test")
        run("{} && pip install -r requirements.txt".format(activate_env))
        run("{} && {} collectstatic --noinput {}".format(activate_env, manage,
                                                         settings_test))
        run("{} && {} migrate {}".format(activate_env, manage, settings_test))
        sudo("service nginx restart", pty=False)
        sudo("service supervisor restart", pty=False)
    print(green("Deploy glup Test successful"))


def create_superuser_test():
    with cd("{}/glup-test".format(home_path)):
        run("{} && {} createsuperuser {}".format(activate_env, manage,
                                                 settings_test))
    print(green("Createsuperuser glup test successful"))


def createsuperuser():
    with cd("{}/glup-backend".format(home_path)):
        run("{} && {} createsuperuser {}".format(activate_env, manage,
                                                 settings))
    print(green("Createsuperuser glup successful"))


def new():
    print("Beginning Glup new Deploy:")
    with cd("{}/glup-new".format(home_path)):
        run("git pull origin new_change")
        run("{} && pip install -r requirements.txt".format(activate_env_new))
        run("{} && {} collectstatic --noinput {}".format(activate_env_new, manage,
                                                         settings_new))
        run("{} && {} migrate {}".format(activate_env_new, manage, settings_new))
        sudo("service nginx restart", pty=False)
        sudo("service supervisor restart", pty=False)
    print(green("Deploy glup New successful"))


def new2():
    print("Beginning Glup new Deploy:")
    with cd("{}/glup-new".format(home_path)):
        sudo("service celery restart", pty=False)
        sudo("service nginx restart", pty=False)
        sudo("service supervisor restart", pty=False)
    print(green("Deploy glup New2 successful"))