from django.conf.urls import url, include
from django.contrib import admin
from django.views.static import serve
from glup.settings import base
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  url(r'^', include('apps.account.urls', namespace='account', app_name='account')),
                  url(r'^api/', include('apps.catalog.urls')),
                  url(r'^api/', include('apps.closet.urls')),
                  url(r'^api/', include('apps.fittingroom.urls')),
                  url(r'^api/', include('apps.coupons.urls')),
                  url(r'^api/', include('apps.notifications.urls')),
                  url(r'^api/', include('apps.outfits.urls')),
                  url(r'^api/', include('apps.socials.urls')),
                  url(r'^docs/', include('rest_framework_docs.urls')),
                  url(r'^media/(?P<path>.*)$', serve, {
                      'document_root': base.MEDIA_ROOT,
                  }),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
