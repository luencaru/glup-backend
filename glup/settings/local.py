from .base import *

APPSECRET_PROOF = False
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'glupdb',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',
    }
}
STATIC_URL = '/static/'
# GRAPH_MODELS = {
#     'applications': ('apps.account',
#                          'apps.catalog',
#                          'apps.closet',
#                          'apps.fittingroom',
#                          'apps.coupons',
#                          'apps.notifications',),
#     'group_models': True,
# }
#
# INSTALLED_APPS += ('django_extensions',)
