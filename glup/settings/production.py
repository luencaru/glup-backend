from .base import *
ALLOWED_HOSTS = ['67.205.138.55']
DEBUG = False
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'glupdb',
        'USER': 'postgres',
        'PASSWORD': 'w74atl4#v_jk',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

'''
PUSH_NOTIFICATIONS_SETTINGS = {
    "GCM_API_KEY": "AIzaSyAU6jXKEkOXC-jptoLeByiFTZRX8uUOJiM",
    "APNS_CERTIFICATE": os.path.join(BASE_DIR, 'BederrFinalPEM_distribution.pem'),
    # "APNS_HOST": "gateway.sandbox.push.apple.com"
}
'''
STATIC_URL = '/static/'
APPSECRET_PROOF = False
ADMINS = (
    ('Erik Admin', 'erikd.guiba@gmail.com'),
)
