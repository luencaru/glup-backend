from .base import *
ALLOWED_HOSTS = ['67.205.138.55','gluptest.kodevian.com', 'gluptest.kodevianstudio.com']
DEBUG=True
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'glupdbtest',
        'USER': 'postgres',
        'PASSWORD': 'w74atl4#v_jk',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

STATIC_URL = '/static/'
APPSECRET_PROOF = False
ADMINS = (
    ('Luis Enrique', 'lucaru9@gmail.com'),
)