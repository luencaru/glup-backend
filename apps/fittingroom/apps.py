from django.apps import AppConfig


class FittingroomConfig(AppConfig):
    name = 'apps.fittingroom'
    verbose_name = 'Probador'
