from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from ..catalog.serializers import ListGarmentSerializer
from ..closet.serializers import ClosetSerializer
from .serializers import *


class ListMyFittingRoomAPIView(generics.ListAPIView):
    '''side choices in query params ('top', 'Superior'), ('middle', 'Medio'), ('down', 'Inferior'), ('accessory', 'Accesorio')'''
    permission_classes = IsAuthenticated,
    serializer_class = ListFRGarmentSerializer

    def get_queryset(self):
        serializer = GarmentFilterSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        if FittingRoom.objects.filter(user=self.request.user).first():
            return FittingRoom.objects.filter(user=self.request.user).first().garments.filter(
                gender=serializer.validated_data.get("gender"), side=serializer.validated_data.get("side"))
        else:
            return []

            # def get_serializer_class(self):
            #     serializer = GarmentFilterSerializer(data=self.request.query_params)
            #     serializer.is_valid(raise_exception=True)
            #     if serializer.validated_data.get("from_the") == 'closet':
            #         return ClosetSerializer
            #     elif serializer.validated_data.get("from_the") == 'catalog':
            #         return ListGarmentSerializer
            #     else:
            #         assert self.serializer_class is not None, (
            #             "'%s' should either include a `serializer_class` attribute, "
            #             "or override the `get_serializer_class()` method."
            #             % self.__class__.__name__
            #         )
