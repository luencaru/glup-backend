from rest_framework import serializers

from ..catalog.models import Garment, Brand
from ..catalog.serializers import CategorySerializer
from .models import FittingRoom


class GarmentSerializer(serializers.Serializer):
    gender = serializers.CharField(required=True)
    side = serializers.CharField(required=True)

    def validate_gender(self, value):
        if value == 'M' or value == 'F':
            return value
        else:
            return serializers.ValidationError("Invalid gender")

    def validate_side(self, value):
        if value == 'top' or value == 'middle' or value == 'down' or value == 'accessory':
            return value
        else:
            return serializers.ValidationError("Invalid side")


class GarmentFilterSerializer(serializers.Serializer):
    gender = serializers.CharField(required=True)
    side = serializers.CharField(required=True)
    # from_the = serializers.CharField(required=True)

    def validate_gender(self, value):
        if value == 'M' or value == 'F':
            return value
        else:
            return serializers.ValidationError("Invalid gender")

    def validate_side(self, value):
        if value == 'top' or value == 'middle' or value == 'down' or value == 'accessory':
            return value
        else:
            return serializers.ValidationError("Invalid side")

            # def validate_from_the(self, value):
            #     return value if value == 'closet' or value == 'catalog' else serializers.ValidationError("Invalid from")


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        exclude = ('id', 'created_at', 'updated_at')


class MyFittingRoomSerializer(serializers.ModelSerializer):
    brand = BrandSerializer()

    class Meta:
        model = Garment
        exclude = ('users',)


class ListFRGarmentSerializer(serializers.ModelSerializer):
    i_like = serializers.SerializerMethodField(read_only=True)
    # my_fitting_room = serializers.SerializerMethodField(read_only=True)
    has_coupon = serializers.SerializerMethodField(read_only=True)
    brand = BrandSerializer()
    category = CategorySerializer()
    discount = serializers.SerializerMethodField(read_only=True)
    # here
    back_photo = serializers.CharField(read_only=True)
    cover = serializers.CharField(read_only=True)
    front_photo = serializers.CharField(read_only=True)
    catalog_photo = serializers.CharField(read_only=True)
    fitting_room_photo = serializers.CharField(read_only=True)

    class Meta:
        model = Garment
        fields = ('id', 'price', 'category', 'name', 'cover', 'back_photo', 'front_photo', 'catalog_photo',
                  'fitting_room_photo', 'i_like', 'likes', 'side', 'has_coupon', 'brand', 'user_brand', 'description',
                  'discount', 'ind3d', 'fitting_room_position', 'color', 'size')

    def get_i_like(self, obj):
        request = self.context.get("request")
        if obj.users.filter(id=request.user.id).exists():
            return True
        else:
            return False

    # def get_my_fitting_room(self, obj):
    #     request = self.context.get("request")
    #     if FittingRoom.objects.filter(user=request.user.id, garments=obj):
    #         return True
    #     else:
    #         return False

    def get_has_coupon(self, obj):
        if obj.garments_stock.exclude(coupon=None).exists():
            return True
        else:
            return False

    def get_discount(self, obj):
        if obj.garments_stock.values('coupon__discount').exists():
            return obj.garments_stock.values('coupon__discount').first().get('coupon__discount')
        else:
            return None
