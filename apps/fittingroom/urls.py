from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^my-fitting-room/$', ListMyFittingRoomAPIView.as_view()),
]
