from django.db import models
from ..account.models import User
from ..catalog.models import Garment


class FittingRoom(models.Model):
    user = models.ForeignKey(User)
    garments = models.ManyToManyField(Garment, blank=True)

    def __str__(self):
        return u'{0}'.format(self.user)
