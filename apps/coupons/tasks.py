from django.template import loader
from django.core.mail.message import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from celery import shared_task


@shared_task
def send_coupon_mail(email, coupon,my_coupon, request):
    protocol = 'https' if request.is_secure() else 'http'
    data = {'user': request.user, 'domain': get_current_site(request), 'coupon': coupon,'my_coupon':my_coupon,'protocol':protocol}
    template = loader.get_template('coupon/coupon_mail.html')
    html = template.render(dict(data))
    subject_user, from_email = 'Cúpon', 'GLUP <email>'
    send_message_async.delay(subject_user, html, from_email, email)


def send_message(subject_user, html, from_email, email):
    message_user = EmailMessage(subject_user, html, from_email, [email])
    message_user.content_subtype = "html"
    message_user.send(fail_silently=True)


send_message_async = shared_task(send_message)
