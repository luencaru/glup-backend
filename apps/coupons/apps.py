from django.apps import AppConfig


class CouponsConfig(AppConfig):
    name = 'apps.coupons'
    verbose_name = 'Mis Cupones'
