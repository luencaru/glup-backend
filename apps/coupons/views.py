from django.core.exceptions import ValidationError
from django.utils.timezone import now
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from apps.coupons.tasks import send_coupon_mail
from .serializers import *
from django.core.validators import validate_email


class ListMyCouponsAPIView(generics.ListAPIView):
    '''
    Lista mis cupones
    '''
    permission_classes = IsAuthenticated,
    serializer_class = StoreLinksSerializer

    def get_queryset(self):
        return self.request.user.mycoupons.filter(garment_stock__coupon__end_date__gte=datetime.today().date())


class AddCouponAPIView(generics.CreateAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = AddCouponSerializer
    queryset = MyCoupons.objects.all()


class DeleteMyCouponAPIView(generics.DestroyAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = MyCouponSerializer
    queryset = MyCoupons.objects.all()


class DetailMyCouponAPIView(generics.RetrieveAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = MyCouponSerializer
    queryset = MyCoupons.objects.all()


class SendCouponAPIView(generics.GenericAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = MailSerializer

    def post(self, request, *args, **kwargs):
        email = self.request.data.get('email')
        code = self.request.data.get('code')
        try:
            validate_email(email)
            coupon = Coupon.objects.filter(id=kwargs.get("pk")).first()
            my_coupon = MyCoupons.objects.filter(code=code).first()
            send_coupon_mail(email, coupon, my_coupon, request)
            return Response({"detail": "OK"}, status=status.HTTP_200_OK)
        except ValidationError:
            raise ValidationError("Invalid email")
