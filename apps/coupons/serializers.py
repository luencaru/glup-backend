from io import BytesIO
import qrcode
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from rest_framework import serializers
from ..catalog.models import Coupon, Garment, Promotion, PromotionalStore, TermsAndConditions, StoreLink, Brand
from ..catalog.serializers import StoreSerializer, SizesSerializer
from .models import MyCoupons
from datetime import datetime
from django.utils.crypto import get_random_string
from django.contrib.sites.shortcuts import get_current_site


class PromotionalStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = PromotionalStore
        exclude = ('created_at', 'updated_at')


class PromotionSerializer(serializers.ModelSerializer):
    promotion_store = PromotionalStoreSerializer()

    class Meta:
        model = Promotion


class TermsAndConditionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsAndConditions


class CouponSerializer(serializers.ModelSerializer):
    promotion = PromotionSerializer()
    terms_and_conditions = TermsAndConditionsSerializer()
    end_date = serializers.DateField(format='%d/%m')
    has_promotion = serializers.SerializerMethodField(read_only=True)
    expire = serializers.SerializerMethodField()
    i_have_this_coupon = serializers.SerializerMethodField(read_only=True)
    code = serializers.SerializerMethodField(read_only=True)
    qr_code = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Coupon
        fields = ('id', 'discount', 'terms_and_conditions', 'end_date', 'promotion', 'has_promotion',
                  'i_have_this_coupon', 'expire', 'code', 'qr_code')

    def get_code(self, val):
        return MyCoupons.objects.filter(user=self.context.get("request").user, garment_stock__coupon=val).first().code

    def get_qr_code(self, val):

        domain = get_current_site(self.context.get('request')).domain
        protocol = 'https' if self.context.get('request').is_secure() else 'http'
        return "{}://{}{}{}".format(protocol, domain, settings.MEDIA_URL,
                                    MyCoupons.objects.filter(user=self.context.get("request").user,
                                                             garment_stock__coupon=val).first().qrcode)

    def get_has_promotion(self, val):
        if val.promotion:
            return True
        else:
            return False

    def get_expire(self, val):
        if val.end_date < datetime.today().date():
            return True
        else:
            return False

    def get_i_have_this_coupon(self, val):
        if MyCoupons.objects.filter(user=self.context.get("request").user, garment_stock__coupon=val).exists():
            return True
        else:
            return False


class MyCouponSerializer(serializers.ModelSerializer):
    coupon = CouponSerializer(source='garment_stock.coupon')

    class Meta:
        model = MyCoupons


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand


class GarmentsSerializer(serializers.ModelSerializer):
    brand = BrandSerializer()

    class Meta:
        model = Garment
        fields = ('id', 'brand', 'price', 'name', 'cover')


class StoreLinksSerializer(serializers.ModelSerializer):
    store = StoreSerializer(source='garment_stock.storelink.store', read_only=True)
    coupon = CouponSerializer(source='garment_stock.coupon', read_only=True)
    sizes_available = SizesSerializer(source='garment_stock.sizes', many=True)
    # my_coupon_id = serializers.SerializerMethodField()
    garment = GarmentsSerializer(source='garment_stock.garment')

    class Meta:
        model = MyCoupons
        fields = (
            'id', 'store', 'coupon', 'sizes_available', 'garment')

        # def get_my_coupon_id(self, val):
        #     request = self.context.get("request")
        #     return MyCoupons.objects.filter(user=request.user, coupon=val.coupon).first().id


class AddCouponSerializer(serializers.ModelSerializer):
    # qr_code = serializers.SerializerMethodField(read_only=True)
    garment_image = serializers.ImageField(source='garment_stock.garment.front_photo', read_only=True)

    class Meta:
        model = MyCoupons
        fields = ('id', 'code', 'qrcode', 'garment_stock', 'garment_image')
        read_only_fields = ('code', 'qrcode', 'garment_image')

    def validate(self, attrs):
        request = self.context.get("request")
        if MyCoupons.objects.filter(user=request.user,
                                    garment_stock__garment=attrs.get("garment_stock").garment).exists():
            raise serializers.ValidationError("You have already this coupon")
        return attrs

    # def get_qr_code(self, val):
    #     domain = get_current_site(self.context.get('request')).domain
    #     protocol = 'https' if self.context.get('request').is_secure() else 'http'
    #     return "{}://{}{}{}".format(protocol, domain, settings.MEDIA_URL,
    #                                 MyCoupons.objects.filter(user=self.context.get("request").user,
    #                                                          coupon=val["coupon"]).first().qrcode)

    def create(self, validated_data):
        request = self.context.get("request")
        code_g = get_random_string(length=8)
        my_coupon = MyCoupons.objects.create(user=request.user, code=code_g,
                                             garment_stock=validated_data['garment_stock'])
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.ERROR_CORRECT_L,
            box_size=10,
            border=4
        )
        data = '{}'.format(my_coupon.code)
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image()
        buffer = BytesIO()
        img.save(buffer)
        filename = 'qrcode-%s.png' % my_coupon.id
        filebuffer = InMemoryUploadedFile(
            buffer, None, filename, 'image/png', None, None)
        my_coupon.qrcode.save(filename, filebuffer)
        my_coupon.save()
        return my_coupon


class MailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    code = serializers.CharField(required=True)
