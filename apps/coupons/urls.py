from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^add-coupon/$', AddCouponAPIView.as_view()),
    url(r'^my-coupons/$', ListMyCouponsAPIView.as_view()),
    url(r'^my-coupon/(?P<pk>\d+)/delete/$', DeleteMyCouponAPIView.as_view()),
    url(r'^my-coupon/(?P<pk>\d+)/$', DetailMyCouponAPIView.as_view()),
    url(r'^coupon/(?P<pk>\d+)/send/$', SendCouponAPIView.as_view()),
]
