from ..account.models import User
from django.db import models
from ..catalog.models import GarmentStock

STATE_CHOICES = (('u', 'usado'), ('n', 'none'))


class MyCoupons(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='mycoupons')
    code = models.CharField(max_length=8, unique=True, blank=True, null=True)
    qrcode = models.ImageField(upload_to='qrcode', blank=True, null=True)
    state = models.CharField(max_length=1, default='n')
    garment_stock = models.ForeignKey(GarmentStock, related_name='mycoupons')

    def __str__(self):
        return u'{0} {1}'.format(self.user, self.garment_stock.coupon)
