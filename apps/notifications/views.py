from fcm_django.models import FCMDevice
from rest_framework.fields import FileField
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from ..closet.models import ClosetShared
from .serializers import *
from rest_framework import generics, status


class FriendshipNotificationAPIView(generics.CreateAPIView):
    serializer_class = CreateFriendshipNotificationSerializer
    permission_classes = IsAuthenticated,

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        vd = serializer.validated_data
        friendship = serializer.save(user_applicant=self.request.user, state=Notifications.States.e.name,
                                     users_receiver=vd.get("users_receiver"), type=Notifications.Types.s.name)
        for user in vd.get('users_receiver'):
            if FCMDevice.objects.filter(user_id=user).exists():
                fcm_devices = FCMDevice.objects.filter(user=user)
                fcm_devices.send_message(title='Glup',
                                         body="{0} ha solicitado ser tu amigo en glup".format(
                                             self.request.user.get_full_name()),
                                         data={"friendship_id": friendship.id, "type": "request",
                                               "photo": self.request.build_absolute_uri(
                                                   self.request.user.photo.url) if self.request.user.photo else None
                                               })


class MyFriendshipNotificationsAPIView(generics.ListAPIView):
    serializer_class = MyFriendshipNotificationsSerializer
    permission_classes = IsAuthenticated,

    def get_queryset(self):
        return Notifications.objects.filter(users_receiver__contains=[self.request.user.id],
                                            enableds__contains=[self.request.user.id, 1])


class AcceptFriendshipNotificationsAPIView(generics.GenericAPIView):
    '''Aceptar Solicitud de amistad '''
    # serializer_class = AcceptFriendshipNotificationSerializer
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        instance = get_object_or_404(
            Notifications.objects.filter(state=Notifications.States.e.name, type=Notifications.Types.s.name,
                                         users_receiver__contains=[self.request.user.id]),
            id=self.kwargs.get('pk'))
        instance.user_applicant.friends.add(request.user)
        instance.user_applicant.save()

        request.user.friends.add(instance.user_applicant)
        request.user.save()
        instance.state = Notifications.States.a.name
        instance.save()

        if FCMDevice.objects.filter(user=instance.user_applicant).exists():
            fcm_devices = FCMDevice.objects.filter(user=instance.user_applicant)
            fcm_devices.send_message(title='Glup',
                                     body="{0} ha aceptado tu solicitud de amistad".format(
                                         request.user.get_full_name()),
                                     data={"user_applicant": request.user.id, "type": "ok_request",
                                           "photo": self.request.build_absolute_uri(
                                               request.user.photo.url) if request.user.photo else None})

        return Response({'detail': 'Solicitud Aceptada'}, status=status.HTTP_200_OK)


class RejectFriendshipNotificationsAPIView(generics.GenericAPIView):
    '''Rechazar Solicitud de amistad '''
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        instance = get_object_or_404(
            Notifications.objects.filter(state=Notifications.States.e.name, type=Notifications.Types.s.name),
            id=self.kwargs.get('pk'))
        instance.delete()
        return Response({'detail': 'Solicitud Rechazada'}, status=status.HTTP_200_OK)


class CreateShareNotificationAPIView(generics.CreateAPIView):
    ''' Crea las notificaciones para compartir prendas fields: users_receiver=[1,2,3,...],garment=idOfGarment(int)
    '''
    serializer_class = CreateShareNotificationSerializer
    permission_classes = IsAuthenticated,

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        users_delete = serializer.validated_data.pop('users_delete')
        serializer.fields.pop('users_delete')
        vd = serializer.validated_data
        if vd.get('users_receiver'):
            notification = serializer.save(user_applicant=self.request.user, garment=vd.get('garment'),
                                           users_receiver=vd.get("users_receiver"), type=Notifications.Types.c.name)

            for user in vd.get('users_receiver'):
                closet, create = ClosetShared.objects.get_or_create(user_received_id=user,
                                                                    user_shared=notification.user_applicant)
                closet.garments.add(notification.garment)

            for user in vd.get('users_receiver'):
                if FCMDevice.objects.filter(user_id=user).exists():
                    fcm_devices = FCMDevice.objects.filter(user=user)
                    fcm_devices.send_message(title='Glup',
                                             body="{0} ha compartido una prenda contigo en glup.".format(
                                                 self.request.user.get_full_name()),
                                             data={"notification_id": notification.id, "type": "request",
                                                   "photo": self.request.build_absolute_uri(
                                                       self.request.user.photo.url) if self.request.user.photo else None,
                                                   "garment_image": self.request.build_absolute_uri(
                                                       notification.garment.front_photo.url)
                                                   })

        if users_delete:
            closets = self.request.user.closets_shared.filter(user_received__in=users_delete)
            for closet in closets:
                closet.garments.remove(vd.get('garment'))


class UpdateShareNotificationAPIView(generics.UpdateAPIView):
    ''' Crea las notificaciones para compartir prendas fields: users_receiver=[1,2,3,...],garment=idOfGarment(int)
    '''
    serializer_class = UpdateShareNotificationSerializer
    permission_classes = IsAuthenticated,
    queryset = Notifications.objects.filter(type=Notifications.Types.c.name)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data['users_receiver'] = eval(request.data.get('users_receiver'))
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class DeactivateNotificationAPI(generics.GenericAPIView):
    ''' sacar notificacion de la lista  id de la notificacion en la url'''
    # serializer_class = AcceptFriendshipNotificationSerializer
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        instance = get_object_or_404(
            Notifications.objects.filter(users_receiver__contains=[self.request.user.id]), id=self.kwargs.get('pk'))
        try:
            instance.enableds.remove([self.request.user.id, 1])
            instance.enableds.append([self.request.user.id, 0])
            instance.save()
            return Response({'detail': 'Notificacion eliminada'}, status=status.HTTP_200_OK)
        except ValueError:
            return Response({'detail': 'No se puede eliminar la notificacion'}, status=status.HTTP_303_SEE_OTHER)
