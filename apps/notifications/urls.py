from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^send-friendship-notification/$', FriendshipNotificationAPIView.as_view()),
    url(r'^my-friendship-notifications/$', MyFriendshipNotificationsAPIView.as_view()),
    url(r'^accept-friendship-notification/(?P<pk>\d+)/$', AcceptFriendshipNotificationsAPIView.as_view()),
    url(r'^reject-friendship-notification/(?P<pk>\d+)/$', RejectFriendshipNotificationsAPIView.as_view()),
    url(r'^send-share-notification/$', CreateShareNotificationAPIView.as_view()),
    url(r'^send-share-notification/(?P<pk>\d+)/$', UpdateShareNotificationAPIView.as_view()),
    url(r'^deactive-notification/(?P<pk>\d+)/$', DeactivateNotificationAPI.as_view()),
]
