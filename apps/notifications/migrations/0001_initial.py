# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-25 02:31
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Notifications',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('users_receiver', django.contrib.postgres.fields.ArrayField(base_field=models.PositiveIntegerField(), blank=True, size=None)),
                ('state', models.CharField(choices=[('a', 'Aceptado'), ('s', 'Enviado')], max_length=1)),
                ('type', models.CharField(choices=[('a', 'Aceptó solicitud de amistad'), ('e', 'Envió solicitud de amistad'), ('c', 'Compartió una prenda'), ('p', 'Prenda procesada con éxito')], default='e', max_length=1)),
                ('user_applicant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='applications', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_at',),
            },
        ),
    ]
