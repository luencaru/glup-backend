# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-25 05:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notifications',
            name='state',
            field=models.CharField(choices=[('a', 'Aceptado'), ('e', 'Enviado')], max_length=1),
        ),
        migrations.AlterField(
            model_name='notifications',
            name='type',
            field=models.CharField(choices=[('s', 'solicitud de amistad'), ('c', 'Compartir una prenda'), ('p', 'Procesar prenda')], default='s', max_length=1),
        ),
    ]
