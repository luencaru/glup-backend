from fcm_django.models import FCMDevice
from rest_framework import serializers
from ..account.models import User
from apps.closet.models import ClosetShared
from ..notifications.models import Notifications


class UserApplicantSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'photo', 'gender', 'profession', 'city', 'country')


class CreateFriendshipNotificationSerializer(serializers.ModelSerializer):
    users_receiver = serializers.ListField(required=False)
    user_applicant = UserApplicantSerializer(read_only=True)

    class Meta:
        model = Notifications
        fields = ('users_receiver', 'id', 'created_at', 'user_applicant', 'state', 'type')
        read_only_fields = ('id', 'created_at', 'user_applicant', 'state', 'type')

    def validate(self, attrs):
        attrs['users_receiver'] = eval(attrs.get('users_receiver')[0])
        request = self.context.get("request")
        if request.user.id in (attrs.get("users_receiver")):
            raise serializers.ValidationError("No puedes mandarte solicitud a ti misma/o")
        elif request.user.friends.filter(id__in=attrs.get("users_receiver")).exists():
            raise serializers.ValidationError("Ya son amigos ")
        elif Notifications.objects.filter(user_applicant=request.user, users_receiver=attrs.get("users_receiver"),
                                          state=Notifications.States.e.name, type=Notifications.Types.s.name).exists():
            raise serializers.ValidationError("Aún no ha respondido tu solicitud")
        return attrs


class MyFriendshipNotificationsSerializer(serializers.ModelSerializer):
    user_applicant = UserApplicantSerializer(read_only=True)

    class Meta:
        model = Notifications
        fields = ('id', 'user_applicant', 'state', 'type')


class AcceptFriendshipNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notifications
        fields = ('state',)


class CreateShareNotificationSerializer(serializers.ModelSerializer):
    users_receiver = serializers.ListField(required=False)
    user_applicant = UserApplicantSerializer(read_only=True)
    users_delete = serializers.ListField(required=False)

    class Meta:
        model = Notifications
        fields = ('id', 'created_at', 'user_applicant', 'users_receiver', 'state', 'type', 'garment', 'users_delete')
        read_only_fields = ('id', 'created_at', 'user_applicant', 'state', 'type', 'users_delete')

    def validate(self, attrs):
        request = self.context.get("request")
        attrs['users_receiver'] = eval(attrs.get('users_receiver')[0]) if attrs.get(
            'users_receiver') else []
        attrs['users_delete'] = eval(attrs.get('users_delete')[0]) if attrs.get(
            'users_delete') else []

        if len(attrs.get('users_receiver')) > 0 and not User.objects.filter(
                id__in=attrs.get('users_receiver')).exists():
            raise serializers.ValidationError("No existe el usuario")
        if request.user.id in (attrs.get("users_receiver")):
            raise serializers.ValidationError("No puedes compartir la prenda a ti misma/o")
        closets = request.user.closets_shared.filter(user_received__in=attrs.get('users_receiver'))
        if closets.exists():
            users = []
            for closet in closets:
                if attrs.get('garment') in closet.garments.all():
                    users.append(closet.user_received)
            if len(users) > 0:
                raise serializers.ValidationError(
                    "Ya compartiste la prenda con {} amigos".format(len(users)))
        return attrs


class UpdateShareNotificationSerializer(serializers.ModelSerializer):
    users_receiver = serializers.ListField(required=True)
    user_applicant = UserApplicantSerializer(read_only=True)

    class Meta:
        model = Notifications
        fields = ('id', 'created_at', 'user_applicant', 'users_receiver', 'state', 'type', 'garment')
        read_only_fields = ('id', 'created_at', 'user_applicant', 'state', 'type', 'garment')

    def update(self, instance, validated_data):
        request = self.context.get("request")
        if request.user.id in (validated_data.get("users_receiver")):
            raise serializers.ValidationError("No puedes compartir la prenda a ti misma/o")

        news = list(set(validated_data.get('users_receiver')) - set(instance.users_receiver))
        olds = list(set(instance.users_receiver) - set(validated_data.get('users_receiver')))
        # now = list(set(instance.users_receiver) & set(validated_data.get('users_receiver')))

        if olds:
            closets = request.user.closets_shared.filter(user_received__in=olds)
            for closet in closets:
                closet.garments.remove(instance.garment)
        if news:
            for user in news:
                closet, create = ClosetShared.objects.get_or_create(user_received_id=user,
                                                                    user_shared=request.user)
                closet.garments.add(instance.garment)

            for user in news:
                if FCMDevice.objects.filter(user_id=user).exists():
                    fcm_devices = FCMDevice.objects.filter(user=user)
                    fcm_devices.send_message(title='Glup',
                                             body="{0} ha compartido una prenda contigo en glup.".format(
                                                 self.request.user.get_fullname()),
                                             data={"notification_id": instance.id, "type": "request"})
        instance.users_receiver = validated_data.get("users_receiver")
        instance.save()
        return instance
