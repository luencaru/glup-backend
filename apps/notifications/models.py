from enum import Enum
from django.contrib.postgres.fields import ArrayField
from django.db import models
from ..account.models import User
from ..catalog.models import Garment


class Notifications(models.Model):
    class States(Enum):
        a = "Aceptado"
        e = "Enviado"

    class Types(Enum):
        s = "solicitud de amistad"
        c = "Compartir una prenda"
        p = "Procesar prenda"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user_applicant = models.ForeignKey(User, related_name='applications')
    users_receiver = ArrayField(models.PositiveIntegerField(), blank=True)
    state = models.CharField(max_length=1, choices=[(item.name, item.value) for item in States], null=True, blank=True)
    type = models.CharField(max_length=1, choices=[(item.name, item.value) for item in Types],
                            default=Types.s.name)
    garment = models.ForeignKey(Garment, related_name='notifications', null=True, verbose_name='Prenda compartida',
                                blank=True)
    enableds = ArrayField(ArrayField(models.IntegerField()), blank=True, null=True)  # [[id,True],[],..]

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return u'{}-{}-{}'.format(self.user_applicant, self.users_receiver, self.type)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.pk and self.users_receiver:
            enableds = []
            for user in self.users_receiver:
                enableds.append([user, 1])
            self.enableds = enableds
        super(Notifications, self).save()

        #    super(Notifications, self).save(update_fields=['code'])
