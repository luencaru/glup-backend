from django.contrib import admin

# Register your models here.
from .models import Notifications


class AdminNotifications(admin.ModelAdmin):
    exclude = ('enableds',)


admin.site.register(Notifications, AdminNotifications)
