from django.contrib import admin

# Register your models here.
from .models import Outfit

admin.site.register(Outfit)
