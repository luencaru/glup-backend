from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^outfits/$', ListCreateOutfitAPI.as_view()),
    url(r'^outfits/(?P<pk>\d+)/$', RetrieveDestroyOutfitAPI.as_view()),
]
