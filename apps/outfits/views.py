from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, DestroyAPIView, get_object_or_404, RetrieveDestroyAPIView, \
    UpdateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import CreateOutfitSerializer, RetrieveOutfitSerializer, UpdateOutfitSerializer


class ListCreateOutfitAPI(ListCreateAPIView):
    '''
        POST: Crear un Outfit ,No enviar user
        GET: List los outfit del user logueado
    '''
    permission_classes = IsAuthenticated,
    serializer_class = CreateOutfitSerializer

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CreateOutfitSerializer
        else:
            return RetrieveOutfitSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        outfit = self.perform_create(serializer)
        headers = self.get_success_headers(RetrieveOutfitSerializer(outfit).data)
        return Response(RetrieveOutfitSerializer(outfit).data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)

    def get_queryset(self):
        return self.request.user.outfits.all()


class RetrieveDestroyOutfitAPI(RetrieveUpdateDestroyAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = RetrieveOutfitSerializer

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return RetrieveOutfitSerializer
        else:
            return UpdateOutfitSerializer

    def get_object(self):
        return get_object_or_404(self.request.user.outfits.all(), pk=self.kwargs.get('pk'))

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        outfit = self.perform_update(serializer)
        return Response(RetrieveOutfitSerializer(outfit).data)

    def perform_update(self, serializer):
        return serializer.save()
