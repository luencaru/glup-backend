from rest_framework import serializers
from ..catalog.serializers import OutfitGarmentDetailSerializer
from .models import Outfit

__author__ = 'lucaru9'


class CreateOutfitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Outfit
        fields = ('id', 'name', 'image', 'description', 'user', 'garments')
        read_only_fields = ('id', 'user')


class RetrieveOutfitSerializer(serializers.ModelSerializer):
    garments = OutfitGarmentDetailSerializer(many=True, read_only=True)

    class Meta:
        model = Outfit
        fields = ('id', 'name', 'image', 'description', 'user', 'garments')


class UpdateOutfitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Outfit
        fields = ('id', 'name', 'image', 'description', 'user', 'garments')
        read_only_fields = ('id', 'user')
