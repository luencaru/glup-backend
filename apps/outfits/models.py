from django.db import models


# Create your models here.
from ..account.models import User
from ..catalog.models import Garment


class Outfit(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=300, verbose_name='Nombre')
    image = models.ImageField(verbose_name='Imagen de Outfit', upload_to='outfits/images', max_length=200, null=True,
                              blank=True)
    description = models.TextField(verbose_name='Descripción del Outfit', blank=True, null=True)
    user = models.ForeignKey(User, related_name='outfits')
    garments = models.ManyToManyField(Garment, verbose_name='Prendas', related_name='outfits')

    def __str__(self):
        return u'OutFit {} de {}  '.format(self.name, self.user)
