from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from rest_framework import serializers
import os
from ..account.serializers import UserInSharedClosetSerializer, FriendsSerializer
from ..account.models import User
from ..catalog.models import Garment, Category
from ..fittingroom.models import FittingRoom
from .models import Closet, ClosetShared
import requests


class MyGarmentSerializer(serializers.ModelSerializer):
    back_photo = serializers.ImageField()
    front_photo = serializers.ImageField()
    category_name = serializers.CharField(max_length=200, required=True, write_only=True)
    category = serializers.CharField(source='category.name', read_only=True)

    class Meta:
        model = Garment
        fields = ('id', 'front_photo', 'back_photo', 'category', 'category_name', 'side', 'fitting_room_position',
                  'gender', 'user_brand', 'size', 'color')
        # exclude = ('garment_type', 'price', 'likes', 'description', 'advice', 'brand',
        #            'users', 'cover', 'name')
        read_only_fields = ('category',)

    def validate_category_name(self, value):
        obj, created = Category.objects.get_or_create(name=value)
        return obj.id

    def create(self, validated_data):
        request = self.context.get("request")
        back_photo_name = validated_data.get("back_photo").name
        front_photo_name = validated_data.get("front_photo").name
        back_photo = os.path.join(settings.MEDIA_ROOT, default_storage.save("catalog_back/{}".format(back_photo_name),
                                                                            validated_data.get("back_photo")))
        front_photo = os.path.join(settings.MEDIA_ROOT,
                                   default_storage.save("catalog_front/{}".format(front_photo_name),
                                                        validated_data.get("front_photo")))
        back_photo_real_name = back_photo.split("/")[-1]
        back_photo_real_name = "catalog_back/{}".format(back_photo_real_name)
        front_photo_real_name = front_photo.split("/")[-1]
        front_photo_real_name = "catalog_front/{}".format(front_photo_real_name)
        validated_data.pop("back_photo")
        validated_data.pop("front_photo")
        # shared_with = validated_data.pop("shared_with")
        category = validated_data.get("category_name")
        validated_data.pop("category_name")
        my_garment = Garment.objects.create(name=request.user.first_name + 'Garment', garment_type="mg",
                                            back_photo=back_photo_real_name,
                                            category_id=category,
                                            front_photo=front_photo_real_name, **validated_data)
        # if shared_with:
        #     for val in shared_with:
        #         my_garment.shared_with.add(val)
        #     my_garment.save()
        closet, created = Closet.objects.get_or_create(user=request.user)
        closet.garment.add(my_garment)
        # files = {'img_a': open('{}'.format(my_garment.front_photo.path), 'rb'),
        #          'img_b': open('{}'.format(my_garment.back_photo.path), 'rb')}
        if my_garment.public:
            # files = {'img_a': my_garment.front_photo.url,
            #          'img_b': my_garment.back_photo.url}
            # files = {'img_a': (f_p.name, f_p.image, f_p.content_type),
            #          'img_b': (b_p.name, b_p.image, b_p.content_type)}
            # res_fp = requests.get(my_garment.front_photo.url)
            # res_bp = requests.get(my_garment.back_photo.url)
            # files = {'img_a': (f_p.name, res_fp.content, f_p.content_type),
            #          'img_b': (b_p.name, res_bp.content, b_p.content_type)}
            # files = {'img_a': (f_p.name, f_p.file.getvalue(), f_p.content_type),
            #          'img_b': (b_p.name, b_p.file.getvalue(), b_p.content_type)}
            values = {'wear_code': my_garment.id, 'user_code': request.user.id, 'wear_side': my_garment.side,
                      'wear_category': my_garment.category.name, 'img_a': my_garment.front_photo.url,
                      'img_b': my_garment.back_photo.url}
            url = 'http://52.55.216.19/api/v2/algorithm-1/'
            # r = requests.post(url, files=files, data=values)
            r = requests.post(url, data=values)
        return my_garment


class PhotosMyGarmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garment
        field = ('id', 'front_photo', 'back_photo')


class PublicSerializer(serializers.Serializer):
    public = serializers.BooleanField(required=True)

    def validate_public(self, value):
        if value is True or value is False:
            return value
        else:
            return serializers.ValidationError("public must be boolean")


class ClosetSerializer(serializers.ModelSerializer):
    my_fitting_room = serializers.SerializerMethodField(read_only=True)
    users_receiver = serializers.SerializerMethodField()
    my_category = serializers.CharField(source='category.name', read_only=True)
    back_photo = serializers.CharField(read_only=True)
    front_photo = serializers.CharField(read_only=True)
    catalog_photo = serializers.CharField(read_only=True)
    fitting_room_photo = serializers.CharField(read_only=True)

    class Meta:
        model = Garment
        fields = ('id', 'front_photo', 'back_photo', 'catalog_photo', 'fitting_room_photo', 'user_brand',
                  'my_fitting_room', 'public', 'ind3d', 'color', 'size', 'garment_type', 'my_category',
                  'users_receiver', 'side', 'description')

    def get_my_fitting_room(self, obj):
        request = self.context.get("request")
        if FittingRoom.objects.filter(user=request.user.id, garments=obj):
            return True
        else:
            return False

    def get_users_receiver(self, obj):
        user = self.context.get('request').user
        users_received = user.closets_shared.filter(garments__in=[obj.id]).values_list('user_received', flat=True)
        return FriendsSerializer(User.objects.filter(id__in=users_received), many=True, context=self.context).data


class UpdateGarmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garment
        fields = ('user_brand', 'color', 'size', 'description')


class TestRequestsSerializer(serializers.Serializer):
    front_photo = serializers.ImageField()
    back_photo = serializers.ImageField()


class GarmentInSharedClosetSerializer(serializers.ModelSerializer):
    my_fitting_room = serializers.SerializerMethodField(read_only=True)
    back_photo = serializers.CharField(read_only=True)
    front_photo = serializers.CharField(read_only=True)
    catalog_photo = serializers.CharField(read_only=True)
    fitting_room_photo = serializers.CharField(read_only=True)

    class Meta:
        model = Garment
        fields = ('id', 'name', 'front_photo', 'back_photo', 'catalog_photo', 'fitting_room_photo', 'user_brand',
                  'my_fitting_room', 'public', 'ind3d', 'color', 'size', 'description')

    def get_my_fitting_room(self, obj):
        request = self.context.get("request")
        if FittingRoom.objects.filter(user=request.user.id, garments=obj):
            return True
        else:
            return False


class SharedClosetsSerializer(serializers.ModelSerializer):
    user_shared = UserInSharedClosetSerializer(read_only=True)
    amount_shared = serializers.SerializerMethodField(read_only=True)
    garments = GarmentInSharedClosetSerializer(many=True, read_only=True)

    class Meta:
        model = ClosetShared
        fields = ('id', 'user_shared', 'amount_shared', 'garments')

    def get_amount_shared(self, obj):
        return obj.garments.count()
