from fcm_django.models import FCMDevice
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import *
import requests
from django.conf import settings


class MyGarmentAPIView(generics.CreateAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = MyGarmentSerializer


class UpdatePhotosMyGarmentAPIView(generics.UpdateAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = PhotosMyGarmentSerializer
    queryset = Garment.objects.all()


class UpdateGarmentAPIView(generics.UpdateAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = UpdateGarmentSerializer
    # queryset = Garment.objects.all()

    def get_queryset(self):
        return Garment.objects.filter(closet__user=self.request.user, garment_type='mg')


class DeleteGarmentAPIView(generics.DestroyAPIView):
    permission_classes = IsAuthenticated,

    def get_queryset(self):
        return Garment.objects.filter(closet__user=self.request.user, garment_type='mg')

    def perform_destroy(self, instance):
        for shared_closet in ClosetShared.objects.filter(user_shared=self.request.user, garments__id=instance.id):
            shared_closet.garments.remove(instance)

            if FCMDevice.objects.filter(user=shared_closet.user_received, active=True).exists():
                fcm_devices = FCMDevice.objects.filter(user=shared_closet.user_received, active=True)
                fcm_devices.send_message(title='Glup',
                                         body="{0} ha eliminado una prenda compartida".format(
                                             self.request.user.get_full_name()),
                                         data={"type": "request", })
        for closet in self.request.user.closet_set.all():
            closet.garment.remove(instance)
        instance.delete()


class ClosetAPIView(generics.ListAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = ClosetSerializer

    def get_queryset(self):
        self.serializer = PublicSerializer(data=self.request.query_params)
        serializer = self.serializer
        serializer.is_valid(raise_exception=True)
        return Garment.objects.filter(closet__user=self.request.user, garment_type='mg')


class TestAPIView(generics.GenericAPIView):
    serializer_class = TestRequestsSerializer

    def post(self, request, *args, **kwargs):
        back_photo = request.data.get("back_photo")
        front_photo = request.data.get("front_photo")
        file = open('{}/catalog_back/PICON_015.png'.format(settings.MEDIA_ROOT), 'rb')
        files = {'upload_file': [file, file]}
        values = {'DB': '-', 'OUT': 'csv', 'SHORT': 'short'}
        url = 'http://67.205.138.55/api/my-garment/1/photos/'
        r = requests.post(url, files=files, data=values)
        print(r)
        return Response({"detail": "OK"}, status=status.HTTP_200_OK)


'''class RetrieveMyGarmentAPIView(generics.UpdateAPIView):
   serializer_class = RetrieveMyGarmentSerializer'''


class MySharedClosetsAPI(generics.ListAPIView):
    '''
        Lista las prendas compartidas    '''
    permission_classes = IsAuthenticated,
    serializer_class = SharedClosetsSerializer

    def get_queryset(self):
        return self.request.user.closets_received.all().order_by('-created_at')
