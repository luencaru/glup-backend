from django.db import models
from apps.account.models import User
from apps.catalog.models import Garment


class Closet(models.Model):
    user = models.ForeignKey(User)
    garment = models.ManyToManyField(Garment, blank=True)

    def __str__(self):
        return u'{0}'.format(self.user)


class ClosetShared(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user_received = models.ForeignKey(User, related_name='closets_received')
    user_shared = models.ForeignKey(User, related_name='closets_shared')
    garments = models.ManyToManyField(Garment, blank=True, related_name='closet_shared')

    def __str__(self):
        return u'comparte:{0} recibe:{1}'.format(self.user_shared, self.user_received)

    class Meta:
        unique_together = ('user_received', 'user_shared')
        verbose_name = "Closet Compartido"
        verbose_name_plural = "Closets Compartidos"
