# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-05 18:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('closet', '0004_auto_20161202_1611'),
    ]

    operations = [
        migrations.AddField(
            model_name='closetshared',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
