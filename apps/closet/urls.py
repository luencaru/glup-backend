from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^my-garment/$', MyGarmentAPIView.as_view()),
    url(r'^my-garment/(?P<pk>\d+)/photos/$', UpdatePhotosMyGarmentAPIView.as_view()),
    url(r'^closet/$', ClosetAPIView.as_view()),
    url(r'^testurl/$', TestAPIView.as_view()),
    url(r'^my-garment/(?P<pk>\d+)/$', UpdateGarmentAPIView.as_view()),
    url(r'^my-garment/(?P<pk>\d+)/delete/$', DeleteGarmentAPIView.as_view()),
    url(r'^my-shared-closets/$', MySharedClosetsAPI.as_view()),

]
