from django.apps import AppConfig


class ClosetConfig(AppConfig):
    name = 'apps.closet'
    verbose_name = 'Closet'
