from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^garments/$', ListGarmentAPIView.as_view()),
    url(r'^garment/(?P<pk>\d+)/$', DetailGarmentAPIView.as_view()),
    url(r'^garment/(?P<pk>\d+)/like-dislike/$', LikeDislikeGarmentAPIView.as_view()),
    url(r'^garment/(?P<pk>\d+)/add-extract/$', AddExtractGarmentAPIView.as_view()),
    url(r'^services/garments/(?P<pk>\d+)/photos/$', PhotosGarmentAPIView.as_view()),
    url(r'^categories/$', ListCategoryAPIView.as_view()),
    url(r'^brands/$', ListBrandAPIView.as_view()),
    url(r'^max_min_prices/$', MaxMinPricesAPIView.as_view()),
    url(r'^services/garments/(?P<pk>\d+)/$', ServiceDetailGarmentAPI.as_view()),
    url(r'^garments/share-mail/$', SendGarmentAPIView.as_view()),
]
