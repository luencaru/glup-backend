from django.contrib import admin

from .models import Garment, Coupon, Promotion, PromotionalStore, Size, Store, Brand, StoreLink, Category, GarmentStock


class GarmentAdmin(admin.ModelAdmin):
    exclude = ('shared_with', 'likes', 'users')


admin.site.register(Promotion)
admin.site.register(PromotionalStore)
admin.site.register(Store)
admin.site.register(Coupon)
admin.site.register(Garment, GarmentAdmin)
admin.site.register(Size)
admin.site.register(Brand)
admin.site.register(StoreLink)
admin.site.register(Category)
admin.site.register(GarmentStock)