# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-17 17:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0018_auto_20161013_1220'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='storelink',
            options={'verbose_name': 'Tienda Enlazada', 'verbose_name_plural': 'Tiendas Enlazadas'},
        ),
        migrations.AddField(
            model_name='store',
            name='headquarter',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='sede'),
        ),
    ]
