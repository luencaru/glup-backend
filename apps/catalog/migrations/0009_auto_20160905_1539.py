# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-05 20:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_auto_20160905_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coupon',
            name='end_date',
            field=models.DateField(blank=True, null=True, verbose_name='fecha de vencimiento'),
        ),
    ]
