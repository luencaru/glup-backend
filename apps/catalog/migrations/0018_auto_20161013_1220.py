# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-13 17:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0017_remove_storelink_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='garment',
            name='store_links',
            field=models.ManyToManyField(blank=True, help_text='tiendas', null=True, related_name='garments', to='catalog.StoreLink', verbose_name='tiendas'),
        ),
    ]
