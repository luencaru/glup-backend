from rest_framework.pagination import PageNumberPagination

__author__ = 'lucaru9'


class Set200Pagination(PageNumberPagination):
    page_size = 200
