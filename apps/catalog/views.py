from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db.models.aggregates import Max, Min
from fcm_django.models import FCMDevice
from rest_framework.generics import get_object_or_404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from .paginations import Set200Pagination
from ..closet.models import Closet
from .tasks import send_garment_mail
from .serializers import *


class ListGarmentAPIView(generics.ListAPIView):
    '''Filtra las prendas por categoria, marca, rango de precios, y género de la prenda, ejemplo
    api/garments/?gender=F&brands=1,2,3&categories=1,2&min_price=1.5&max_price=200'''
    permission_classes = IsAuthenticated,
    serializer_class = ListGarmentSerializer

    def get_queryset(self):
        self.serializer = GarmentFilterSerializer(data=self.request.query_params)
        serializer = self.serializer
        serializer.is_valid(raise_exception=True)
        queryset = Garment.objects.filter(public=True, garment_type='sg',
                                          gender=serializer.validated_data.get("gender"))
        if (not serializer.validated_data.get("brands") and not serializer.validated_data.get(
                "categories") and not serializer.validated_data.get("min_price") and not serializer.validated_data.get(
            "max_price")):
            return queryset
        elif (not serializer.validated_data.get("brands") and not serializer.validated_data.get("categories")) and (
                    serializer.validated_data.get("min_price") and (serializer.validated_data.get("max_price"))):
            return queryset.filter(
                price__range=(serializer.validated_data.get("min_price"), serializer.validated_data.get("max_price")))
        elif (serializer.validated_data.get("brands") and not serializer.validated_data.get("categories")) and (
                    not serializer.validated_data.get("min_price") and (
                        not serializer.validated_data.get("max_price"))):
            return queryset.filter(brand__in=serializer.validated_data.get("brands"))
        elif (not serializer.validated_data.get("brands") and serializer.validated_data.get("categories")) and (
                    not serializer.validated_data.get("min_price") and (
                        not serializer.validated_data.get("max_price"))):
            return queryset.filter(category__in=serializer.validated_data.get("categories"))
        elif (serializer.validated_data.get("brands") and serializer.validated_data.get("categories")) and (
                    not serializer.validated_data.get("min_price") and (
                        not serializer.validated_data.get("max_price"))):
            return queryset.filter(category__in=serializer.validated_data.get("categories"),
                                   brand__in=serializer.validated_data.get("brands"))
        elif (serializer.validated_data.get("brands") and not serializer.validated_data.get("categories")) and (
                    serializer.validated_data.get("min_price") and (
                        serializer.validated_data.get("max_price"))):
            return queryset.filter(
                price__range=(serializer.validated_data.get("min_price"), serializer.validated_data.get("max_price")),
                brand__in=serializer.validated_data.get("brands"))
        elif (not serializer.validated_data.get("brands") and serializer.validated_data.get("categories")) and (
                    serializer.validated_data.get("min_price") and (
                        serializer.validated_data.get("max_price"))):
            return queryset.filter(
                price__range=(serializer.validated_data.get("min_price"), serializer.validated_data.get("max_price")),
                category__in=serializer.validated_data.get("categories"))
        else:
            return queryset.filter(
                price__range=(serializer.validated_data.get("min_price"), serializer.validated_data.get("max_price")),
                brand__in=serializer.validated_data.get("brands"),
                category__in=serializer.validated_data.get("categories"))


class DetailGarmentAPIView(generics.RetrieveAPIView):
    '''Detalle de prenda'''
    permission_classes = IsAuthenticated,
    serializer_class = DetailGarmentSerializer

    def get_object(self):
        return Garment.objects.filter(id=self.kwargs['pk']).first()


class LikeDislikeGarmentAPIView(APIView):
    '''me gusta si el usuario no le ha dado me gusta, sino quitar el like'''
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        garment = get_object_or_404(Garment.objects.all(),
                                    id=self.kwargs.get('pk'))
        if not (garment.users.filter(id=request.user.id).exists()):
            garment.likes += 1
            garment.users.add(request.user)
        elif garment.users.filter(id=request.user.id).exists():
            garment.likes -= 1
            garment.users.remove(request.user)
        garment.save()
        return Response(status=status.HTTP_200_OK)


class AddExtractGarmentAPIView(APIView):
    '''Agregar prenda a mi probador, o quitarlo si ya está dentro'''
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        garment = get_object_or_404(Garment.objects.all(),
                                    id=self.kwargs.get('pk'))
        fit, create = FittingRoom.objects.get_or_create(user=request.user)
        if not (fit.garments.filter(id=garment.id).exists()):
            fit.garments.add(garment)
            fit.save()
            return Response("Prenda agregada al probador", status=status.HTTP_200_OK)
        else:
            fit.garments.remove(garment)
            fit.save()
            return Response("Prenda retirada al probador", status=status.HTTP_200_OK)


class PhotosGarmentAPIView(generics.RetrieveUpdateAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()
    serializer_class = PhotosGarmentSerializer
    queryset = Garment.objects.all()

    def perform_update(self, serializer):
        instance = serializer.save()
        user = get_object_or_404(Closet.objects.all(), garment=instance).user
        if FCMDevice.objects.filter(user=user).exists():
            fcm_devices = FCMDevice.objects.filter(user=user)
            fcm_devices.send_message(title='Glup',
                                     body="Prenda procesada con éxito.",
                                     data={"type": "request"})


class ListCategoryAPIView(generics.ListAPIView):
    '''Devuelve la lista de categorías de las prendas'''
    permission_classes = IsAuthenticated,
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    pagination_class = Set200Pagination


class ListBrandAPIView(generics.ListAPIView):
    '''Devuelve la listas de marcas de las prendas'''
    permission_classes = IsAuthenticated,
    serializer_class = BrandSerializer
    queryset = Brand.objects.all()
    pagination_class = Set200Pagination


class MaxMinPricesAPIView(generics.GenericAPIView):
    '''Devuelve el precio maximo y precio mínimo que existe entre las prendas para los filtros'''
    # permission_classes = IsAuthenticated,

    def get(self, request, *args, **kwargs):
        garments = Garment.objects.all()
        if garments.exists():
            prices = garments.aggregate(Max("price"), Min("price"))
            return Response({'max_price': int(prices['price__max']), 'min_price': int(prices['price__min'])},
                            status=status.HTTP_200_OK)
        else:
            return Response({'max_price': 0, 'min_price': 0}, status=status.HTTP_200_OK)


class SendGarmentAPIView(generics.GenericAPIView):
    '''
        Comparti prenda por correo Body: garment(id), email
    '''
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        email = self.request.data.get('email')
        garment = get_object_or_404(Garment.objects.all(), id=self.request.data.get('garment'))
        try:
            validate_email(email)
            send_garment_mail(email, garment, request)
            return Response({"detail": "OK"}, status=status.HTTP_200_OK)
        except ValidationError:
            raise ValidationError("Invalid email")


class ServiceDetailGarmentAPI(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()
    serializer_class = ServiceGarmentSerializer
    queryset = Garment.objects.all()
