from django.db import models
from apps.account.models import User
from django.utils.crypto import get_random_string

SIDE_CHOICES = (
    ('top', 'Superior'), ('middle', 'Medio'), ('down', 'Inferior'), ('accessory', 'Accesorio'))

SIZE_CHOICES = (
    ('xxl', 'XXL'), ('xl', 'XL'), ('l', 'Large'), ('m', 'Medium'), ('s', 'Small'), ('xs', 'XS'), ('xss', 'XXS'),)

FITTING_ROOM_POSITIONS_CHOICES = (
    ('top', 'superior'), ('bottom', 'inferior'), ('middle', 'medio'), ('accessory', 'accesorio'), ('shoes', 'zapatos'),
    ('special', 'especial'),)


def obtain_code():
    return get_random_string(length=8)


class Category(models.Model):
    name = models.CharField(max_length=30, verbose_name='nombre', unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Categorías'
        verbose_name = 'Categoría'

    def __str__(self):
        return u'{}'.format(self.name)


class PromotionalStore(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50, verbose_name='nombre')
    logo = models.ImageField(upload_to='pslogos')

    class Meta:
        verbose_name_plural = "Tiendas para Promociones"
        verbose_name = "Tienda para Promoción"

    def __str__(self):
        return u'{0}'.format(self.name)


class Store(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50, verbose_name='nombre')
    headquarter = models.CharField(max_length=50, verbose_name='sede', null=True, blank=True)
    address = models.CharField(max_length=100, verbose_name='dirección')
    logo = models.ImageField(upload_to='slogos')

    class Meta:
        ordering = 'name',
        verbose_name = 'Tienda'
        verbose_name_plural = 'Tiendas'

    def __str__(self):
        return u'{0} - {1}'.format(self.name, self.headquarter)


class Size(models.Model):
    size = models.CharField(max_length=10, choices=SIZE_CHOICES, unique=True)

    class Meta:
        verbose_name_plural = 'Tamaños'
        verbose_name = 'Tamaño'

    def __str__(self):
        return u'{0}'.format(self.size)


class Brand(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100, verbose_name="Nombre de la marca")
    image = models.ImageField(upload_to="brands", null=True, blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Marca de Ropa'
        verbose_name_plural = 'Marcas de Ropa'

    def __str__(self):
        return u'{0}'.format(self.name)


class Garment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    garment_type = models.CharField(max_length=2, default='sg',
                                    choices=(('mg', 'Mi prenda'), ('sg', 'Prenda en tienda')), null=True, blank=True)
    public = models.BooleanField(default=True)
    shared_with = models.ManyToManyField(User, related_name='shared_with_me', blank=True)
    side = models.CharField(max_length=15, null=True, choices=SIDE_CHOICES, verbose_name='sitio')
    fitting_room_position = models.CharField(max_length=15, choices=FITTING_ROOM_POSITIONS_CHOICES, null=True,
                                             blank=True, verbose_name='posición en el probador')

    gender = models.CharField(max_length=1, null=True, choices=(('F', 'Femenino'), ('M', 'Masculino')),
                              verbose_name='genero')
    category = models.ForeignKey(Category, related_name='garments', verbose_name='categoría', null=True, blank=True)
    brand = models.ForeignKey(Brand, related_name='garments', verbose_name='marca de ropa', null=True, blank=True)
    user_brand = models.CharField(verbose_name='marca', max_length=100, null=True, blank=True)
    name = models.CharField(max_length=60, verbose_name='nombre', null=True, blank=True)
    price = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='precio', null=True, blank=True)
    cover = models.ImageField(max_length=200, upload_to='catalog_covers', verbose_name='imagen del cover', null=True,
                              blank=True)
    front_photo = models.ImageField(max_length=200, upload_to='catalog_front', verbose_name='imagen frontal')
    back_photo = models.ImageField(max_length=200, upload_to='catalog_back', verbose_name='imagen trasera')
    catalog_photo = models.ImageField(max_length=200, upload_to='catalog_photo', verbose_name='imagen en catalogo',
                                      null=True,
                                      blank=True)
    fitting_room_photo = models.ImageField(upload_to='fitting_room_photo', verbose_name='imagen en provador', null=True,
                                           blank=True, max_length=200)
    likes = models.PositiveIntegerField(default=0)
    users = models.ManyToManyField(User, related_name='gusers', blank=True)
    description = models.TextField(verbose_name='descripción', null=True, blank=True)
    advice = models.TextField(verbose_name='recomendaciones', null=True, blank=True)
    color = models.CharField(max_length=60, null=True, blank=True)
    # store_links = models.ManyToManyField('StoreLink', blank=True, related_name='garments',
    #                                      verbose_name="tiendas", help_text='tiendas')
    size = models.CharField(max_length=24, verbose_name='tamaño', null=True, blank=True)
    ind3d = models.BooleanField(default=False, verbose_name='En 3D')
    texture_path = models.CharField(max_length=400, verbose_name='Ruta de textura', null=True, blank=True)
    geometry = models.CharField(max_length=400, verbose_name='Geometría', null=True, blank=True)
    number_images = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Prendas'
        verbose_name = 'Prenda'
        ordering = '-created_at',

    def __str__(self):
        return u'{0}'.format(self.name)


class Promotion(models.Model):
    promotion = models.CharField(max_length=200, verbose_name='promoción')
    time = models.CharField(max_length=150, null=True, blank=True,
                            help_text="válido del 31 de mayo al 31 de octubre o agotar stock")
    description = models.TextField(verbose_name='descripción', blank=True, null=True)
    promotion_store = models.ForeignKey(PromotionalStore, verbose_name='tienda promocional')

    class Meta:
        verbose_name_plural = 'Promociones'
        verbose_name = 'Promoción'

    def __str__(self):
        return u'{0}'.format(self.promotion)


class TermsAndConditions(models.Model):
    first_text = models.TextField(verbose_name='primer texto')
    second_text = models.TextField(verbose_name='segundo texto')
    third_text = models.TextField(verbose_name='tercer texto')


class Coupon(models.Model):
    discount = models.CharField(max_length=20, verbose_name='descuento', help_text='50% o 2x1')
    end_date = models.DateField(null=True, blank=True, verbose_name='fecha de vencimiento')
    promotion = models.ForeignKey(Promotion, null=True, blank=True)
    terms_and_conditions = models.ForeignKey(TermsAndConditions, null=True, blank=True,
                                             verbose_name='terminos y condiciones', related_name='coupons',
                                             help_text='LLenar solo si no hay promoción')
    quantity = models.PositiveIntegerField(default=0, null=True, blank=True)
    message = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'Cupon'
        verbose_name_plural = 'Cupones'

    def __str__(self):
        return u'{0} {1}'.format(self.end_date, self.discount)


class StoreLink(models.Model):
    store = models.ForeignKey(Store, related_name='links', verbose_name='Tienda')
    link = models.URLField()
    # sizes_available = models.ManyToManyField(Size, related_name='size_store', verbose_name='tamaños disponibles')
    # coupon = models.ForeignKey(Coupon, null=True, blank=True, related_name='store_links', verbose_name='coupon')
    # is_enabled = models.BooleanField(default=True, verbose_name='Tienda enlazada habilitada')

    class Meta:
        verbose_name = 'Tienda Enlazada'
        verbose_name_plural = 'Tiendas Enlazadas'

    def __str__(self):
        return u'{}'.format(self.store)


class GarmentStock(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_enabled = models.BooleanField(default=True, verbose_name='Prenda habilitada')
    sizes = models.ManyToManyField(Size, related_name='garments_stock', verbose_name='Tallas')
    garment = models.ForeignKey(Garment, related_name='garments_stock', verbose_name='Prenda')
    storelink = models.ForeignKey(StoreLink, related_name='garments_stock', verbose_name='Tienda Enlazada')
    coupon = models.ForeignKey(Coupon, related_name='garments_stock', null=True, blank=True, verbose_name='Cupon')

    def __str__(self):
        return u'Tienda enlazada:{}  Prenda:{}'.format(self.storelink, self.garment)
