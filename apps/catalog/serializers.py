from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from easy_thumbnails.files import get_thumbnailer
from rest_framework import serializers
from datetime import datetime
from apps.coupons.models import MyCoupons
from .models import Garment, Coupon, Store, Size, Promotion, PromotionalStore, Brand, StoreLink, Category, \
    TermsAndConditions, GarmentStock
from ..fittingroom.models import FittingRoom


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        exclude = ('created_at', 'updated_at')


class GarmentFilterSerializer(serializers.Serializer):
    gender = serializers.CharField(required=True)
    categories = serializers.CharField(required=False)
    brands = serializers.CharField(required=False)
    min_price = serializers.CharField(required=False)
    max_price = serializers.CharField(required=False)

    def validate_categories(self, value):
        return Category.objects.filter(id__in=value.split(","))

    def validate_brands(self, value):
        return Brand.objects.filter(id__in=value.split(","))

    def validate_min_price(self, value):
        try:
            value = float(value)
            return value
        except:
            return serializers.ValidationError("invalid min price")

    def validate_max_price(self, value):
        try:
            value = float(value)
            return value
        except:
            return serializers.ValidationError("invalid max price")

    def validate_gender(self, value):
        if value == 'M' or value == 'F':
            return value
        else:
            return serializers.ValidationError("invalid gender")


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category


class ListGarmentSerializer(serializers.ModelSerializer):
    i_like = serializers.SerializerMethodField(read_only=True)
    my_fitting_room = serializers.SerializerMethodField(read_only=True)
    has_coupon = serializers.SerializerMethodField(read_only=True)
    brand = BrandSerializer()
    category = CategorySerializer()
    discount = serializers.SerializerMethodField(read_only=True)
    link = serializers.SerializerMethodField(read_only=True)
    # here
    back_photo = serializers.CharField(read_only=True)
    cover = serializers.CharField(read_only=True)
    front_photo = serializers.CharField(read_only=True)
    catalog_photo = serializers.CharField(read_only=True)
    fitting_room_photo = serializers.CharField(read_only=True)

    class Meta:
        model = Garment
        fields = ('id', 'price', 'category', 'name', 'cover', 'back_photo', 'front_photo', 'catalog_photo',
                  'fitting_room_photo', 'i_like', 'my_fitting_room', 'likes', 'side', 'has_coupon', 'brand',
                  'discount', 'ind3d', 'link', 'public')

    def get_i_like(self, obj):
        request = self.context.get("request")
        if obj.users.filter(id=request.user.id).exists():
            return True
        else:
            return False

    def get_my_fitting_room(self, obj):
        request = self.context.get("request")
        if FittingRoom.objects.filter(user=request.user.id, garments=obj):
            return True
        else:
            return False

    def get_has_coupon(self, obj):
        if obj.garments_stock.exclude(coupon=None).exists():
            return True
        else:
            return False

    def get_discount(self, obj):
        if obj.garments_stock.values('coupon__discount').exists():
            discounts = obj.garments_stock.values_list('coupon__discount', flat=True)
            offers = []
            percentages = []
            for discount in discounts:
                discount = discount.lower()
                if 'x' in discount:
                    offers.append(discount)
                else:
                    percentages.append(discount)
            if len(offers) > 0:
                aux = '0x0'
                for offer in offers:
                    offer = offer.replace('s/', '')
                    if int(aux.split('x')[0]) < int(offer.split('x')[0]):
                        aux = offer
                    if int(aux.split('x')[0]) == int(offer.split('x')[0]):
                        if int(aux.split('x')[1]) <= int(offer.split('x')[1]):
                            pass
                        else:
                            aux = offer
                ret = aux.split('x')
                return '{}x S/.{}'.format(ret[0], ret[1])

            else:
                aux = []
                for percentage in percentages:
                    aux.append(percentage.split('%')[0])
                aux.sort(reverse=True)

                return str(aux[0] + '%')

        else:
            return None

    def get_link(self, obj):
        if obj.garments_stock.values('storelink__link').exists():
            return obj.garments_stock.values('storelink__link').first().get('storelink__link')
        else:
            return None


class PromotionalStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = PromotionalStore
        exclude = ('created_at', 'updated_at')


class PromotionSerializer(serializers.ModelSerializer):
    promotion_store = PromotionalStoreSerializer()

    class Meta:
        model = Promotion


class SizesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ('size',)


class StoreSerializer(serializers.ModelSerializer):
    logo = serializers.CharField(read_only=True)

    class Meta:
        model = Store
        fields = ('id', 'name', 'address', 'logo', 'headquarter')


class StoreLinkSerializer(serializers.ModelSerializer):
    store = StoreSerializer(read_only=True)

    class Meta:
        model = StoreLink
        fields = ('id', 'link', 'store')


class TermsAndConditionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsAndConditions


class CouponSerializer(serializers.ModelSerializer):
    promotion = PromotionSerializer(read_only=True)
    terms_and_conditions = TermsAndConditionsSerializer()
    has_promotion = serializers.SerializerMethodField(read_only=True)
    end_date = serializers.DateField(format='%d/%m/%Y')
    expire = serializers.SerializerMethodField()
    i_have_this_coupon = serializers.SerializerMethodField(read_only=True)
    code = serializers.SerializerMethodField(read_only=True)
    qr_code = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Coupon
        fields = ('id', 'discount', 'terms_and_conditions', 'end_date', 'promotion', 'has_promotion',
                  'i_have_this_coupon', 'expire', 'qr_code', 'code')

    def get_code(self, val):
        if MyCoupons.objects.filter(user=self.context.get("request").user, garment_stock__coupon=val).exists():
            return MyCoupons.objects.filter(user=self.context.get("request").user,
                                            garment_stock__coupon=val).first().code
        else:
            return None

    def get_qr_code(self, val):
        if MyCoupons.objects.filter(user=self.context.get("request").user, garment_stock__coupon=val).exists():
            domain = get_current_site(self.context.get('request')).domain
            protocol = 'https' if self.context.get('request').is_secure() else 'http'
            return "{}://{}{}{}".format(protocol, domain, settings.MEDIA_URL,
                                        MyCoupons.objects.filter(user=self.context.get("request").user,
                                                                 garment_stock__coupon=val).first().qrcode)
        else:
            return None

    def get_has_promotion(self, val):
        if val.promotion:
            return True
        else:
            return False

    def get_expire(self, val):
        if val.end_date < datetime.today().date():
            return True
        else:
            return False

    def get_i_have_this_coupon(self, val):
        if MyCoupons.objects.filter(user=self.context.get("request").user, garment_stock__coupon=val).exists():
            return True
        else:
            return False


class GarmentStockSerializer(serializers.ModelSerializer):
    storelink = StoreLinkSerializer(read_only=True)
    coupon = CouponSerializer(read_only=True)
    sizes = SizesSerializer(many=True)

    class Meta:
        model = GarmentStock
        fields = ('id', 'is_enabled', 'sizes', 'coupon', 'storelink')


class DetailGarmentSerializer(serializers.ModelSerializer):
    my_fitting_room = serializers.SerializerMethodField(read_only=True)
    brand = BrandSerializer()
    garments_stock = GarmentStockSerializer(many=True)
    category = CategorySerializer()
    # here
    back_photo = serializers.CharField(read_only=True)
    cover = serializers.CharField(read_only=True)
    front_photo = serializers.CharField(read_only=True)

    class Meta:
        model = Garment
        fields = (
            'id', 'cover', 'back_photo', 'front_photo', 'my_fitting_room', 'brand', 'name', 'description',
            'advice', 'garments_stock', 'category', 'name', 'price', 'gender')

    def get_my_fitting_room(self, obj):
        request = self.context.get("request")
        if FittingRoom.objects.filter(user=request.user.id, garments=obj):
            return True
        else:
            return False


class PhotosGarmentSerializer(serializers.ModelSerializer):
    front_photo = serializers.CharField(max_length=500, required=True)
    back_photo = serializers.CharField(max_length=500, required=True)
    catalog_photo = serializers.CharField(max_length=500, required=False)
    fitting_room_photo = serializers.CharField(max_length=500, required=False)
    cover = serializers.CharField(max_length=500, required=False)

    class Meta:
        model = Garment
        fields = ('id', 'front_photo', 'back_photo', 'catalog_photo', 'fitting_room_photo', 'cover')


class MaxMinPricesSerializer(serializers.Serializer):
    min_price = serializers.CharField(read_only=True)
    max_price = serializers.CharField(read_only=True)


class SendGarmentMailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class ServiceGarmentSerializer(serializers.ModelSerializer):
    # image = serializers.SerializerMethodField()
    brand = BrandSerializer()
    front_photo = serializers.CharField()

    class Meta:
        model = Garment
        fields = ('id', 'brand', 'name', 'description', 'advice', 'texture_path', 'geometry', 'front_photo')

        # def get_image(self, obj):
        #     if obj.front_photo:
        #         return obj.front_photo.url
        #     else:
        #         return None


class OutfitGarmentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garment
        fields = ('id', 'front_photo', 'side', 'fitting_room_position', 'gender', 'name', 'fitting_room_photo')
