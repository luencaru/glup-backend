# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-16 20:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0013_auto_20161124_1755'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='photo',
            field=models.ImageField(blank=True, max_length=300, null=True, upload_to='user'),
        ),
    ]
