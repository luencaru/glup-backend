from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^api/register/$', CreateUserAPIView.as_view()),
    url(r'^api/user/(?P<pk>\d+)/photo/$', UpdateUserPhotoAPIView.as_view()),
    url(r'^api/users/$', FilterUsersAPIView.as_view()),
    url(r'^api/users/add-categories/$', AddCategoriesUserAPI.as_view()),
    url(r'^api/my-friends/$', MyFriendsAPIView.as_view()),
    url(r'^api/login/$', LoginAPIView.as_view(), name='login'),
    url(r'^api/login-store/$', LoginStoreAPIView.as_view(), name='login'),
    url(r'^api/user/retrieve/$', RetrieveUserAPIView.as_view()),
    url(r'^api/user/synchronize/$', CreateSocialUserFacebookAPI.as_view()),
    url(r'^api/login/mobile/(?P<backend>[^/]+)/$', FacebookMobileLoginAPI.as_view(), name="facebook-mobile-login"),
    url(r'^api/user/update/$', UpdateUserAPIView.as_view()),
    url(r'^api/user/change-password/$', ChangePasswordAPIView.as_view()),
    url(r'^api/me/devices/fcm/$', RegisterFCMDeviceAPI.as_view(), name='register-fcm-device'),
    url(r'^api/me/devices/fcm/update/$', UpdateFCMDeviceAPI.as_view(), name='update-fcm-device'),
    url(r'^api/country/(?P<pk>\d+)/cities$', CityAPIView.as_view()),
    url(r'^api/countries/$', CountryAPIView.as_view()),
    url(
        r'^api/user/recovery/(?P<id>\d+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm,
        {"post_reset_redirect": reverse_lazy(
            'account:password_reset_complete')},
        name='password_reset_confirm'),
    url(r'^api/reset/done/$', password_reset_complete,
        name='password_reset_complete'),
    url(r'^api/user/recovery/$', RecoveryPasswordAPI.as_view()),
    url(r'^api/friends/facebook/$', ListNotFriendsGlupAPI.as_view()),
    url(r'^api/friends/delete/$', DeleteFriendAPI.as_view()),
    url(r'^api/invitations/send/$', SendInvitationAPI.as_view()),
    url(r'^register/web/$', CreateEmailUserView.as_view(), name='register_web'),
    url(r'^welcome/$', DownloadAPPView.as_view(), name='download_app'),
    url(r'^services/users/$', UpdatePassAPI.as_view()),

]
