from django import forms
from apps.account.models import User

__author__ = 'lucaru9'
CHOICES = (('M', 'Masculino'), ('F', 'Femenino'))


class CreateUserForm(forms.ModelForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    gender = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)

    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'gender', 'cellphone', 'birth_date')

    def clean_password(self):
        if len(self.cleaned_data.get('password')) < 6:
            self.add_error('password', 'Mínimo 6 caracteres.')
        else:
            return self.cleaned_data.get('password')

    def clean_email(self):  # check if username dos not exist before

        if User.objects.filter(email=self.cleaned_data['email']).exists():
            self.add_error('email', 'El email ya ha sido registrado.')
        else:
            return self.cleaned_data['email']

    def save(self, commit=True):
        user = User.objects.create(
            email=self.cleaned_data.get('email'),
            first_name=self.cleaned_data.get('first_name'),
            last_name=self.cleaned_data.get('last_name'),
            gender=self.cleaned_data.get('gender'),
            cellphone=self.cleaned_data.get('cellphone'),
            birth_date=self.cleaned_data.get('birth_date')
        )

        user.set_password(self.cleaned_data.get('password'))
        user.save()
        return user
