from django.contrib.auth.models import PermissionsMixin, BaseUserManager, \
    AbstractBaseUser
from django.contrib.postgres.fields import ArrayField
from django.db import models

MODE_USER_CHOICES = (('u', 'usuario'), ('t', 'administrador de tienda'), ('s', 'super administrador'))
POST_CATEGORIES = (('look', 'Look'), ('beauty', 'Belleza'), ('phrases', 'Frases'), ('culture', 'Cultura'),
                   ('diy', 'Diy'), ('destinations', 'Destinos'))


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser,
                     **extra_fields):
        user = self.model(email=email, is_active=True,
                          is_staff=is_staff, is_superuser=is_superuser,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, username, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    cellphone = models.CharField(max_length=9, blank=True, null=True)
    gender = models.CharField(max_length=1, choices=(('M', 'Male'), ('F', 'Female')), null=True, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='user', blank=True, null=True, max_length=300)
    profession = models.CharField(max_length=30, null=True, blank=True)
    country = models.CharField(max_length=30, null=True, blank=True)
    city = models.CharField(max_length=30, null=True, blank=True)
    friends = models.ManyToManyField("User", blank=True)
    mode_user = models.CharField(max_length=1, default='u', choices=MODE_USER_CHOICES, verbose_name='tipo de usuario')
    store = models.ForeignKey('catalog.Store', null=True, blank=True, verbose_name='tienda asociada',
                              related_name='adminstore')
    post_categories = ArrayField(models.CharField(max_length=12, choices=POST_CATEGORIES), blank=True, default=[])

    objects = UserManager()
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'

    def get_user_face(self):
        if self.social_auth.count() > 0:
            return True
        else:
            return False

    def get_full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        return '{0}'.format(self.first_name)

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"


class Country(models.Model):
    name = models.CharField(max_length=30, verbose_name='nombre', unique=True)

    class Meta:
        ordering = 'name',
        verbose_name = 'Pais'
        verbose_name_plural = 'Paises'

    def __str__(self):
        return u'{}'.format(self.name)


class City(models.Model):
    name = models.CharField(max_length=30, verbose_name='nombre')
    country = models.ForeignKey(Country, verbose_name='pais', related_name='cities')

    class Meta:
        ordering = ('name',)
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'

    def __str__(self):
        return u'{} - {}'.format(self.country, self.name)
