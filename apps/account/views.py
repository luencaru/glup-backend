import json
from braces.views import AnonymousRequiredMixin
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import deprecate_current_app
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import transaction
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import SetPasswordForm
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.template.response import TemplateResponse
from django.shortcuts import resolve_url, redirect
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import CreateView, TemplateView
from django.views.generic.edit import ModelFormMixin
from rest_framework import generics, status, filters
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView, CreateAPIView, get_object_or_404, UpdateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from social.apps.django_app.default.models import UserSocialAuth
from social.apps.django_app.utils import psa
from .forms import CreateUserForm
from .tasks import recovery_password_mail, send_invitation_mail
from .serializers import *


class CreateUserAPIView(generics.CreateAPIView):
    """ Registro de usuario
        gender=(M o F)
    """
    serializer_class = CreateUserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class RetrieveUserAPIView(generics.RetrieveAPIView):
    '''Devuelve el usuario logueado'''
    serializer_class = RetrieveUserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class UpdateUserAPIView(generics.UpdateAPIView):
    '''Actualizar usuario'''
    permission_classes = IsAuthenticated,
    serializer_class = UpdateUserSerializer
    queryset = User.objects.all()

    def get_object(self):
        return self.request.user


class UpdateUserPhotoAPIView(generics.UpdateAPIView):
    '''Actualizer fotos del usuario'''
    permission_classes = IsAuthenticated,
    serializer_class = UpdateUserPhotoSerializer
    queryset = User.objects.all()


class ChangePasswordAPIView(generics.GenericAPIView):
    '''Cambiar contraseña para usuario logueado'''
    permission_classes = IsAuthenticated,
    serializer_class = ChangePasswordSerializer

    def put(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={"user": request.user})
        serializer.is_valid(raise_exception=True)

        serializer.save()
        return Response({"detail": "OK"}, status=status.HTTP_200_OK)


class LoginAPIView(generics.GenericAPIView):
    '''login api'''
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.get_user())
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class LoginStoreAPIView(generics.GenericAPIView):
    serializer_class = LoginStoreSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.get_user())
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class MobileLoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.get_user())
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class FacebookMobileLoginAPI(MobileLoginAPI):
    '''Facebook Login'''
    serializer_class = FacebookLoginSerializer

    @method_decorator(psa('account:facebook-mobile-login'))
    def dispatch(self, request, *args, **kwargs):
        return super(FacebookMobileLoginAPI, self).dispatch(request, *args, **kwargs)


class RecoveryPasswordAPI(generics.GenericAPIView):
    '''Recuperar contraseña'''
    serializer_class = PasswordRecoverySerializer
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data
        recovery_password_mail(vd.get("email"), request)
        return Response({"detail": "OK"}, status=status.HTTP_200_OK)


class RegisterFCMDeviceAPI(generics.CreateAPIView):
    '''Registrar Dispositivo especificanco si es android o ios en el campo type'''
    serializer_class = FCMDeviceSerializer
    pagination_class = None
    permission_classes = IsAuthenticated,

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data
        device_id = serializer.validated_data.get("device_id")
        device = FCMDevice.objects.filter(device_id=device_id).first()
        if device:
            device.user = self.request.user
            device.name = vd.get("name")
            device.registration = vd.get("registration_id")
            device.save()
            return Response(FCMDeviceSerializer(device).data, status=status.HTTP_201_CREATED)
        else:
            serializer.save(user=self.request.user)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UpdateFCMDeviceAPI(generics.UpdateAPIView):
    '''
        Enviar device_id y registration_id(token de android)
    '''
    permission_classes = IsAuthenticated,
    serializer_class = UpdateFCMDeviceSerializer

    def get_object(self):
        return get_object_or_404(FCMDevice.objects.filter(user=self.request.user),
                                 device_id=self.request.data.get('device_id'))


class MyFriendsAPIView(generics.ListAPIView):
    '''List all my friends'''
    permission_classes = IsAuthenticated,
    serializer_class = FriendsSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name', 'last_name')

    def get_queryset(self):
        return self.request.user.friends.all()


class FilterUsersAPIView(generics.ListAPIView):
    '''Filtrar usuarios por query params buscando por coincidencias en nombre o apellidos,ejemplo api/users/?search=erik'''
    queryset = User.objects.filter(is_superuser=False)
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name', 'last_name')
    permission_classes = IsAuthenticated,


class CityAPIView(generics.ListAPIView):
    '''devuelve una lista de las ciudades que le corresponden a ese pais'''
    serializer_class = CitySerializer
    pagination_class = None

    def get_queryset(self):
        return City.objects.filter(country__id=self.kwargs.get("pk"))


class CountryAPIView(generics.ListAPIView):
    '''Devuelve una lista de los paises subidos desde el administrador'''
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    pagination_class = None


# 4 views for password reset:
# - password_reset sends the mail
# - password_reset_done shows a success message for the above
# - password_reset_confirm checks the link the user clicked and
#   prompts for a new password
# - password_reset_complete shows a success message for the above

# Doesn't need csrf_protect since no-one can guess the URL
@sensitive_post_parameters()
@never_cache
@deprecate_current_app
def password_reset_confirm(request, id=None, token=None,
                           template_name='registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm,
                           post_reset_redirect=None,
                           extra_context=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert id is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        user = User.objects.get(pk=id)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('Enter new password')
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = _('Password reset unsuccessful')
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


@deprecate_current_app
def password_reset_complete(request,
                            template_name='registration/password_reset_complete.html',
                            extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL),
        'title': _('Password reset complete'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


class ListNotFriendsGlupAPI(ListAPIView):
    ''' List User Facebook example http://127.0.0.1:8000/api/friends/facebook/?uids=676213222452455,55465456456456 '''
    permission_classes = (IsAuthenticated,)
    serializer_class = ListAddFacebookFriendSerializer

    def get_queryset(self):
        return UserSocialAuth.objects.filter(uid__in=self.request.GET.get('uids').split(','))


class CreateSocialUserFacebookAPI(CreateAPIView):
    '''
    Enviar campos uid y access_token
    '''
    permission_classes = (IsAuthenticated,)
    serializer_class = CreateSocialUserSerializer


class SendInvitationAPI(generics.GenericAPIView):
    '''
        Enviar invitacion de registro por correo: garment(id), email
    '''
    permission_classes = IsAuthenticated,

    def post(self, request, *args, **kwargs):
        email = self.request.data.get('email')
        if not User.objects.filter(email=email).exists():
            try:
                validate_email(email)
                send_invitation_mail(email, request)
                return Response({"detail": "OK"}, status=status.HTTP_200_OK)
            except ValidationError:
                return Response({"detail": "Email invalid"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({"detail": "OK"}, status=status.HTTP_204_NO_CONTENT)


class CreateEmailUserView(AnonymousRequiredMixin, CreateView):
    """ Registro de usuario
        gender=(M o F)
    """
    form_class = CreateUserForm
    template_name = 'accounts/users/register.html'
    success_url = reverse_lazy('account:download_app')
    authenticated_redirect_url = reverse_lazy('account:download_app')

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        self.request.session['user_name'] = self.object.first_name
        # login(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())


class DownloadAPPView(TemplateView):
    template_name = 'accounts/users/downloads.html'

    def get_context_data(self, **kwargs):
        context = super(DownloadAPPView, self).get_context_data(**kwargs)
        context['user_name'] = self.request.session.get('user_name', 'usuario')
        return context


class DeleteFriendAPI(generics.GenericAPIView):
    ''' Eliminar de mis amigos '''
    # serializer_class = AcceptFriendshipNotificationSerializer
    permission_classes = IsAuthenticated,

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        instance = get_object_or_404(request.user.friends.all(), id=request.data.get('user_id'))
        instance.friends.remove(request.user)
        instance.save()
        request.user.friends.remove(instance)
        request.user.save()
        if request.user.closets_shared.filter(user_received=instance).exists():
            closet = request.user.closets_shared.filter(user_received=instance).first()
            closet.delete()
        return Response({'detail': 'Amigo eliminado'}, status=status.HTTP_200_OK)


class UpdatePassAPI(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        user = get_object_or_404(User.objects.all(), email=request.data.get('email'))
        if len(request.data.get('password')) > 0:
            user.set_password(request.data.get('password'))
            user.save()
            return Response({'detail': 'password actualizado'}, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'password en blanco'}, status=status.HTTP_400_BAD_REQUEST)


class AddCategoriesUserAPI(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserAddCategoriesSerializer

    def get_object(self):
        return self.request.user
