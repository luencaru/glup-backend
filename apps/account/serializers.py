from django.contrib.auth import authenticate
from fcm_django.models import FCMDevice
from requests.exceptions import HTTPError
from rest_framework.authtoken.models import Token
from rest_framework import serializers
from social.apps.django_app.default.models import UserSocialAuth
from apps.account.models import City, Country
from apps.closet.models import Closet
from apps.fittingroom.models import FittingRoom
from ..account.models import User
from ..notifications.models import Notifications


class UserSerializer(serializers.ModelSerializer):
    invitation_sent = serializers.SerializerMethodField(read_only=True)
    is_my_friend = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'gender', 'birth_date', 'photo', 'profession', 'city',
                  'country', 'is_my_friend', 'invitation_sent')

    def get_is_my_friend(self, obj):
        return True if self.context.get('request').user.friends.filter(id=obj.id).exists() else False

    def get_invitation_sent(self, obj):
        return True if self.context.get('request').user.applications.filter(state=Notifications.States.e.name,
                                                                            type=Notifications.Types.s.name,
                                                                            users_receiver__contains=[
                                                                                obj.id]).exists() else False


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'gender', 'cellphone', 'birth_date')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        FittingRoom.objects.get_or_create(user=user)
        Closet.objects.get_or_create(user=user)
        Token.objects.create(user=user)
        return user


class RetrieveUserSerializer(serializers.ModelSerializer):
    birth_date = serializers.DateField(format='%d/%m/%Y')
    uid = serializers.SerializerMethodField()
    photo = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'gender', 'cellphone', 'birth_date', 'photo', 'mode_user',
                  'profession', 'city', 'country', 'uid')

    def get_uid(self, obj):
        return obj.social_auth.filter(provider='facebook').last().uid if obj.social_auth.count() > 0 else None


class UpdateUserSerializer(serializers.ModelSerializer):
    birth_date = serializers.DateField(format='%d/%m/%Y', required=False)

    class Meta:
        model = User
        fields = (
            'first_name', 'last_name', 'gender', 'cellphone', 'birth_date', 'profession', 'city', 'country')


class UpdateUserPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('photo',)


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(
        error_messages={"blank": "Este campo es obligatorio"})
    new_password = serializers.CharField(
        error_messages={"blank": "Este campo es obligatorio"})
    email = serializers.EmailField(
        error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        user = self.context.get("user")
        if attrs.get("email") != user.email:
            raise serializers.ValidationError({"email": "Email mismatch"})
        if not user.check_password(attrs.get("old_password")):
            raise serializers.ValidationError({"password": "Password mismatch"})
        return attrs

    def save(self, **kwargs):
        user = self.context.get("user")
        user.set_password(self.validated_data.get("new_password"))
        user.save()


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(error_messages={"blank": "Este campo es obligatorio"})
    password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        self.user_cache = authenticate(email=attrs["email"], password=attrs["password"])
        if not self.user_cache:
            raise serializers.ValidationError("Invalid login")
        return attrs

    def get_user(self):
        return self.user_cache


class LoginStoreSerializer(serializers.Serializer):
    email = serializers.EmailField(error_messages={"blank": "Este campo es obligatorio"})
    password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        self.user_cache = authenticate(email=attrs["email"], password=attrs["password"])
        if not self.user_cache:
            raise serializers.ValidationError("Invalid login")
        elif self.user_cache.mode_user == 't':
            return attrs
        else:
            raise serializers.ValidationError("User is not store admin")

    def get_user(self):
        return self.user_cache


class FacebookLoginSerializer(serializers.Serializer):
    access_token = serializers.CharField(
        error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        request = self.context.get("request")
        self.user_cache = None
        try:
            self.user_cache = request.backend.do_auth(attrs.get("access_token"))
            return attrs
        except HTTPError:
            raise serializers.ValidationError("Invalid facebook token")

    def get_user(self):
        return self.user_cache


class PasswordRecoverySerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        self.cached_user = User.objects.filter(email=value).first()
        if not self.cached_user:
            raise serializers.ValidationError("The email is not registered")
        return value


class FriendsSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'photo', 'profession', 'country', 'city', 'gender')


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name',)


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'name')


class FCMDeviceSerializer(serializers.ModelSerializer):
    registration_id = serializers.CharField(required=True)
    device_id = serializers.CharField(required=True)

    def validate_registration_id(self, attr):
        user = getattr(self.context.get("request"), "user")
        try:
            if type(eval(attr)).__name__ == 'dict':
                return eval(attr).get('token')
            else:
                raise serializers.ValidationError("Registration id inválido")
        except SyntaxError:
            return attr

    class Meta:
        model = FCMDevice
        fields = ('id', 'user', 'device_id', 'registration_id', 'name', 'type')


class UpdateFCMDeviceSerializer(serializers.ModelSerializer):
    registration_id = serializers.CharField(required=True)
    device_id = serializers.CharField(required=True)

    def validate_device_id(self, attr):
        user = getattr(self.context.get("request"), "user")
        if not self.Meta.model.objects.filter(device_id=attr, user=user).exists():
            raise serializers.ValidationError("El usuario no ha registrado este dispositivo")
        return attr

    class Meta:
        model = FCMDevice
        fields = ('id', 'user', 'device_id', 'registration_id', 'name', 'type')
        read_only_fields = ('user', 'name', 'type', 'device_id')


class UserInAddFacebookSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'photo', 'profession', 'country', 'city', 'gender')


class ListAddFacebookFriendSerializer(serializers.ModelSerializer):
    is_my_friend = serializers.SerializerMethodField(read_only=True)
    invitation_sent = serializers.SerializerMethodField(read_only=True)
    user = UserInAddFacebookSerializer(read_only=True)

    class Meta:
        model = UserSocialAuth
        fields = ('user', 'is_my_friend', 'invitation_sent')

    def get_is_my_friend(self, obj):
        return True if self.context.get('request').user.friends.filter(id=obj.user.id).exists() else False

    def get_invitation_sent(self, obj):
        return True if self.context.get('request').user.applications.filter(state=Notifications.States.e.name,
                                                                            type=Notifications.Types.s.name,
                                                                            users_receiver__contains=[
                                                                                obj.user.id]).exists() else False


class ListUidFacebookSerializer(serializers.Serializer):
    uid = serializers.CharField(read_only=True)


class UserInSharedClosetSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'photo', 'profession', 'gender', 'country', 'city')


class CreateSocialUserSerializer(serializers.ModelSerializer):
    access_token = serializers.CharField(max_length=500)

    class Meta:
        model = UserSocialAuth
        fields = ('user', 'provider', 'uid', 'extra_data', 'access_token')
        read_only_fields = ('user', 'provider', 'extra_data')

    def create(self, validated_data):
        extra_data = {"access_token": validated_data.get('access_token'),
                      "id": validated_data.get('uid'), "token_type": None, "expires": None}
        instance = UserSocialAuth.objects.create(user=self.context.get('request').user, provider='facebook',
                                                 uid=validated_data.get('uid'), extra_data=extra_data)
        return instance


class UserBasicSocialSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'photo')


class UserAddCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('post_categories',)
