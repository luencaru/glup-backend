__author__ = 'lucaru9'
from django.conf.urls import url
from .views import CreatePostAPI, CreateCommentAPI, LikeDislikeCommentAPI, ReportPostAPI, SavePostAPI, \
    ListPostForCategory

urlpatterns = [
    url(r'^posts/$', ListPostForCategory.as_view()),
    # url(r'^api/users/(?P<pk>\d+)/posts/$', ListPostAPI.as_view()),
    url(r'^posts/create/$', CreatePostAPI.as_view()),
    url(r'^posts/(?P<pk>\d+)/comment/$', CreateCommentAPI.as_view()),
    url(r'^posts/(?P<pk>\d+)/like-dislike/$', LikeDislikeCommentAPI.as_view()),
    url(r'^posts/(?P<pk>\d+)/report/$', ReportPostAPI.as_view()),
    url(r'^posts/(?P<pk>\d+)/save/$', SavePostAPI.as_view()),
    # url(r'^api/posts/(?P<pk>\d+)/delete/$', DeletePostAPI.as_view()),
    # url(r'^api/posts/comment/(?P<pk>\d+)/delete/$', DeleteCommentAPI.as_view()),
]
