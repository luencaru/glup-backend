from enum import Enum
from django.contrib.postgres.fields import ArrayField
from django.db import models

# Create your models here.
# encoding= utf-8
from django.db import models
from ..account.models import User


class Post(models.Model):
    class Categories(Enum):
        look = "Look"
        beauty = "Belleza"
        phrases = "Frases"
        culture = "Cultura"
        diy = "Diy"
        destinations = "Destinos"

    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='posts/images', null=True, blank=True, verbose_name='Imagen del post')
    title = models.CharField(max_length=300, verbose_name='Título del post')
    text = models.TextField(blank=True, null=True, verbose_name='Texto del post')
    is_enable = models.BooleanField(default=True)
    users_likes = models.ManyToManyField(User, related_name='posts_likes', verbose_name='Likes del post')
    user = models.ForeignKey(User, related_name='posts', verbose_name='Usuario', null=True)
    users_save = models.ManyToManyField(User, related_name='posts_saved', verbose_name='Usuarios que guardaron el post')
    users_report = models.ManyToManyField(User, related_name='posts_reported',
                                          verbose_name='Usuarios que reportaron el post')
    categories = ArrayField(models.CharField(max_length=12, choices=[(item.name, item.value) for item in Categories]),
                            blank=True, default=[])

    class Meta:
        verbose_name_plural = "Posts"
        verbose_name = "Post"
        ordering = ['-created_at', ]

    def __str__(self):
        if self.user:
            return u'Post de {}'.format(self.user.get_full_name())
        else:
            return u'Post de GLUP {}'.format(self.created_at)


class Comment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    text = models.TextField(verbose_name='Texto del comentario')
    user = models.ForeignKey(User, related_name='comments', verbose_name='Usuario')
    post = models.ForeignKey(Post, related_name='comments', verbose_name='Post')

    class Meta:
        verbose_name_plural = "Comentarios"
        verbose_name = "Comentario"
        ordering = ['-created_at', ]
