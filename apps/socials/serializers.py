from rest_framework import serializers
from ..account.serializers import UserBasicSocialSerializer
from .models import Post, Comment

__author__ = 'lucaru9'


class CreatePostSerializer(serializers.ModelSerializer):
    user = UserBasicSocialSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'created_at', 'title', 'image', 'text', 'is_enable', 'user', 'categories')


class CreateCommentsSerializer(serializers.ModelSerializer):
    user = UserBasicSocialSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'created_at', 'text', 'user')
        read_only_fields = ('user', 'created_at')


class ListCommentInPostSerializer(serializers.ModelSerializer):
    user = UserBasicSocialSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'created_at', 'text', 'user')


class ListPostSerializer(serializers.ModelSerializer):
    user = UserBasicSocialSerializer(read_only=True)
    is_my_post = serializers.SerializerMethodField(read_only=True)
    is_liked = serializers.SerializerMethodField(read_only=True)
    is_saved = serializers.SerializerMethodField(read_only=True)
    likes = serializers.SerializerMethodField(read_only=True)
    number_comments = serializers.SerializerMethodField(read_only=True)
    comments = ListCommentInPostSerializer(read_only=True, many=True)

    class Meta:
        model = Post
        fields = ('id', 'created_at', 'title', 'image', 'text', 'is_enable', 'user', 'is_my_post', 'is_liked', 'likes',
                  'is_saved', 'number_comments', 'comments')

    def get_is_my_post(self, obj):
        band = False
        if self.context.get('request').user:
            user = self.context.get('request').user
            if user.is_authenticated():
                if obj.user == user:
                    band = True
        return band

    def get_is_liked(self, obj):
        band = False
        if self.context.get('request').user.is_authenticated():
            user = self.context.get('request').user
            if user in obj.users_likes.all():
                band = True
        return band

    def get_is_saved(self, obj):
        band = False
        if self.context.get('request').user.is_authenticated():
            user = self.context.get('request').user
            if user in obj.users_save.all():
                band = True
        return band

    def get_likes(self, obj):
        return obj.users_likes.count()

    def get_number_comments(self, obj):
        return obj.comments.count()
