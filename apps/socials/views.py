from django.shortcuts import render


# Create your views here.
from rest_framework import status
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Post
from .serializers import CreatePostSerializer, CreateCommentsSerializer, ListPostSerializer


class ListPostForCategory(ListAPIView):
    '''
    List Post filter for category api/post/?category=categoria
    List Post for User categories api/post/  response = ('id', 'created_at', 'title', 'image', 'text', 'is_enable', 'user', 'is_my_post', 'is_liked', 'likes',
                  'is_saved', 'number_comments', 'comments')
    '''
    permission_classes = IsAuthenticated,
    serializer_class = ListPostSerializer

    def get_queryset(self):
        if self.request.query_params.get('category'):
            category = self.request.query_params.get('category')
            return Post.objects.filter(categories__contains=[category])
        else:
            categories = self.request.user.post_categories
            return Post.objects.filter(categories__contains=categories)


class CreatePostAPI(CreateAPIView):
    '''
    Create Post request fields : 'title', 'image', 'text'
    response fields: 'id', 'created_at', 'title', 'image', 'text', 'is_enable', 'user'(Basic User)
    '''
    permission_classes = IsAuthenticated,
    serializer_class = CreatePostSerializer

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)


class CreateCommentAPI(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CreateCommentsSerializer

    def perform_create(self, serializer):
        post = get_object_or_404(Post.objects.all(is_enable=True), id=self.kwargs.get('pk'))
        return serializer.save(user=self.request.user, post=post)


class LikeDislikeCommentAPI(APIView):
    '''
    Post Like (status=200) o dislike (status=202)
    '''
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        post = get_object_or_404(Post.objects.all(), id=self.kwargs.get('pk'))
        if self.request.user in post.users_likes.all():
            post.users_likes.remove(self.request.user)
            post.save()
            return Response({'detail': 'DisLike ok'}, status=status.HTTP_202_ACCEPTED)
        else:
            post.users_likes.add(self.request.user)
            post.save()
            return Response({'detail': 'Like ok'}, status=status.HTTP_200_OK)


class SavePostAPI(APIView):
    '''
    Save post
    '''
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        post = get_object_or_404(Post.objects.all(), id=self.kwargs.get('pk'))
        if self.request.user in post.users_likes.all():
            post.users_likes.remove(self.request.user)
            post.save()
            return Response({'detail': 'DisLike ok'}, status=status.HTTP_202_ACCEPTED)
        else:
            post.users_likes.add(self.request.user)
            post.save()
            return Response({'detail': 'Like ok'}, status=status.HTTP_200_OK)


class ReportPostAPI(APIView):
    '''
    Report post
    '''
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        post = get_object_or_404(Post.objects.all(), id=self.kwargs.get('pk'))
        if self.request.user in post.users_report.all():
            return Response({'detail': 'Post already reported'}, status=status.HTTP_202_ACCEPTED)
        else:
            post.users_report.add(self.request.user)
            post.save()
            return Response({'detail': 'Post reported'}, status=status.HTTP_200_OK)
